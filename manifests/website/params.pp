# Default parameters shared by all Tails website builders and webservers
class tails::website::params () {

  $production_slave_languages = {
    'de' => 'Deutsch',
    'es' => 'Español',
    'fr' => 'Français',
    'it' => 'Italiano',
    'pt' => 'Português',
    'ru' => 'Русский',
  }

  $weblate_additional_languages = {
    'ar'      => 'Arabic',
    'ca'      => 'Catalan',
    'fa'      => 'Farsi',
    'id'      => 'Indonesian',
    'pl'      => 'Polish',
    'sr_Latn' => 'Serbian(Latin)',
    'tr'      => 'Turkish',
    'zh'      => 'Chinese',
    'zh_TW'   => 'Chinese(Taiwan)',
  }

  $weblate_slave_languages = deep_merge($production_slave_languages, $weblate_additional_languages)

}
