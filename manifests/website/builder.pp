# Manage what's needed to build the Tails website
class tails::website::builder (
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^stretch|buster|bullseye|sid$/ {
    fail('This class only supports Debian Stretch, Buster, Bullseye, and sid.')
  }

  ### Resources

  $stretch_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  #6907
  package { 'ikiwiki':
    ensure  => present,
    require => Apt::Pin['ikiwiki'],
  }
  apt::pin { 'ikiwiki':
    ensure     => $stretch_and_older_only_ensure,
    packages   => 'ikiwiki',
    originator => 'Debian',
    codename   => 'buster',
    priority   => 990,
  }

  #17005
  package { 'po4a':
    ensure  => present,
    require => Apt::Pin['po4a'],
  }
  if $::lsbdistcodename == 'stretch' {
    apt::pin { 'po4a':
      ensure     => present,
      packages   => 'po4a',
      originator => 'Debian Backports',
      codename   => 'stretch-backports',
      priority   => 990,
    }
  } elsif $::lsbdistcodename == 'buster' {
    apt::pin { 'po4a':
      ensure     => present,
      packages   => 'po4a',
      originator => 'Debian',
      codename   => 'buster',
      priority   => 990,
    }
  } elsif $::lsbdistcodename == 'bullseye' {
    apt::source { 'buster':
      ensure   => present,
      location => $tails::apt::debian_url,
      release  => 'buster',
      repos    => $tails::apt::repos,
      pin      => {
        originator => 'Debian',
        codename   => 'buster',
        priority   => 1,
      },
    }
    apt::pin { 'po4a':
      ensure     => present,
      packages   => 'po4a',
      originator => 'Debian',
      codename   => 'buster',
      priority   => 1000,
    }
  }

  $packages = [
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'perlmagick',
    'ruby',
  ]

  ensure_packages($packages)
}
