# Manage time-based snapshots of one APT repository in a reprepro setup.
#
# This resource is meant to be used from tails::reprepro::snapshots::time_based,
# and is not designed to be used in isolation.
#
# Resource name: the upstream repository we are mirroring.
#
# Note: the ensure parameter is not fully supported, because some resources
# we are managing here don't provide this functionality.
define tails::reprepro::snapshots::time_based::repository (
  Hash $architectures,
  Stdlib::Absolutepath $basedir,
  Stdlib::Absolutepath $homedir,
  String $signwith,
  String $user,
  Enum['present', 'absent'] $ensure             = 'present',
  Enum['present', 'absent'] $automatic_refresh  = 'present',
  Integer[0, 23] $automatic_refresh_delta_hours = 0,
  Integer[0, 59] $automatic_refresh_delta_mins  = 0,
  Pattern[/\A\S+\z/] $distributions_template    = "tails/reprepro/snapshots/time_based/${name}/distributions.erb",
  Pattern[/\A\S+\z/] $updates_template          = "tails/reprepro/snapshots/time_based/${name}/updates.erb",

) {

  ### Sanity checks

  assert_private()
  validate_re($name, '^[a-zA-Z-]+$')

  ### Resources

  $repository = $name

  reprepro::repository { "tails-time-based-snapshots-${repository}":
    architectures                => $architectures,
    user                         => $user,
    group                        => $user,
    uploaders                    => [],
    basedir                      => $basedir,
    origin                       => "Time-based snapshots of ${repository} APT repository",
    basedir_mode                 => '0751',
    incoming_mode                => '0700',
    signwith                     => $signwith,
    manage_distributions_conf    => true,
    distributions_template       => $distributions_template,
    manage_incoming_conf         => false,
    handle_incoming_with_inotify => false,
    manage_updates_conf          => true,
    updates_template             => $updates_template,
    index_template               => 'tails/reprepro/index.html.erb',
  }

  # Exec[/usr/local/bin/reprepro-export-key ...] is run as long as this file
  # does not exist. Unfortunately, that script (and much of the reprepro
  # Puppet module, really) assumes that $HOME == $basedir, which is not the case
  # for us, so it will never succeed at exporting the key. Let's prevent that
  # script from ever running, we don't need what it produces anyway.
  file { "${basedir}/key.asc":
    ensure => $ensure,
    owner  => root,
    group  => $user,
    mode   => '0440',
  }

  $cronjob_ensure = $ensure ? {
    present => $automatic_refresh,
    absent  => absent,
  }

  $directory_ensure = $ensure ? {
    present => directory,
    absent  => absent,
  }

  tails::reprepro::snapshots::time_based::cronjob { "${repository}-1":
    ensure      => $cronjob_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 1,
    repository  => $repository,
    basedir     => $basedir,
  }
  tails::reprepro::snapshots::time_based::cronjob { "${repository}-2":
    ensure      => $automatic_refresh,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 2,
    repository  => $repository,
    basedir     => $basedir,
  }
  tails::reprepro::snapshots::time_based::cronjob { "${repository}-3":
    ensure      => $cronjob_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 3,
    repository  => $repository,
    basedir     => $basedir,
  }
  tails::reprepro::snapshots::time_based::cronjob { "${repository}-4":
    ensure      => $cronjob_ensure,
    delta_hours => $automatic_refresh_delta_hours,
    delta_mins  => $automatic_refresh_delta_mins,
    id          => 4,
    repository  => $repository,
    basedir     => $basedir,
  }

  tails::reprepro::snapshots::secret_keys { "time_based_${name}":
    basedir => $basedir,
    homedir => $homedir,
    user    => $user,
  }

  file { "${basedir}/project":
    ensure => $directory_ensure,
    owner  => $user,
    group  => $user,
    mode   => '0755',
  }

  file { [
    "${basedir}/project/packages.db.size",
    "${basedir}/project/references.db.size",
  ]:
    ensure => $ensure,
    owner  => $user,
    group  => $user,
    mode   => '0644',
  }

}
