# Default parameters that are shared between tails::reprepro::* classes.
class tails::reprepro::params () {

  $snapshots_architectures = {
    'buster'       => ['amd64', 'source'],
    'bullseye'     => ['amd64', 'source'],
    'sid'          => ['amd64', 'source'],
    'experimental' => ['amd64', 'source'],
    'tails'        => ['amd64', 'source'],
    'torproject'   => ['amd64'],
  }

  $signing_key = '221F9A3C6FA3E09E182E060BC7988EA7A358D82E'

  $snapshots_signwith = $signing_key

}
