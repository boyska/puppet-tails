# Make a system ready to build Tails ISO and USB images.
class tails::iso_builder (
  Enum['present', 'absent'] $ensure = 'present',
) {

  package { 'diffoscope':
    ensure  => $ensure,
  }

  $blocked_uneeded_diffoscope_recommends = [
    'androguard',
    'apksigner',
    'fp-utils',
    'procyon-decompiler',
    'r-base-core',
    'r-base-dev',
    'r-recommended',
    'u-boot-tools',
    'xmlbeans',
  ]

  # Avoid diffoscope pulling in too many dependencies
  apt::pin { 'blocked_unneeded_diffoscope_recommends':
    packages   => $blocked_uneeded_diffoscope_recommends,
    originator => 'Debian',
    priority   => -1,
  }
  package { $blocked_uneeded_diffoscope_recommends: ensure => purged }

  $builder_packages = [
    'dnsmasq-base',
    'ebtables',
    'faketime',
    'git',
    'libvirt-daemon-system',
    'pigz',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
    'vmdb2',
  ]

  ensure_packages(
    $builder_packages,
    {'ensure' => $ensure}
  )

}
