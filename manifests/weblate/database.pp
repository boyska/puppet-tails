# Configure the database for the translation platform
class tails::weblate::database(
  String $postgres_password,
  Integer[1] $max_client_conn = 200,
  String $bind_address = '127.0.0.1',
) inherits tails::weblate::params {

  ### PostgreSQL database configuration

  class { 'postgresql::server':
    listen_addresses => $bind_address,
  }

  postgresql::server::db { $postgres_database:
    user     => $postgres_user,
    password => postgresql::postgresql_password($postgres_user, $postgres_password),
  }

  postgresql::server::pg_hba_rule { 'allow Weblate container to access Weblate database':
    description => "Open up PostgreSQL for access from ${bind_address}",
    type        => 'host',
    database    => $postgres_database,
    user        => $postgres_user,
    address     => "${bind_address}/32",
    auth_method => 'md5',
  }

  postgresql::server::extension { 'pg_trgm':
    database => $postgres_database,
  }

  ### PgBouncer -- a PostgreSQL pooling mechanism

  ensure_packages(['pgbouncer'])

  file { '/etc/pgbouncer/pgbouncer.ini':
    ensure  => present,
    owner   => postgres,
    group   => postgres,
    mode    => '0640',
    require => Package['pgbouncer'],
    content => epp('tails/weblate/pgbouncer.ini.epp', {
      postgres_password => $postgres_password,
      max_client_conn   => $max_client_conn,
    }),
  }

  $md5_password_hash = md5("${postgres_password}${postgres_user}")

  file { '/etc/pgbouncer/userlist.txt':
    ensure  => present,
    owner   => postgres,
    group   => postgres,
    mode    => '0640',
    require => Package['pgbouncer'],
    content => "\"${postgres_user}\" \"md5${md5_password_hash}\"",
  }

  service { 'pgbouncer':
    ensure    => running,
    subscribe => [
      File['/etc/pgbouncer/pgbouncer.ini'],
      File['/etc/pgbouncer/userlist.txt'],
    ],
  }

  ### Backups

  include tails::backupninja

  file { '/etc/backup.d/10.pgsql':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0400',
    content => "hotcopy = no
sqldump = yes
backupdir = /var/backups/postgres
databases = all
compress = yes
format = plain
",
    require => Package['backupninja'],
  }

}
