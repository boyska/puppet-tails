# Patches for parts of the system so that it works as we need
class tails::weblate::patches {

  # We patch Ikiwiki's po plugin so it:
  #
  #  - is more informative when it fails to build a page, and
  #  - continues building after it finds a problem.
  #
  # TODO: will need review when there's a newer version of Ikiwiki available

  file { '/usr/share/perl5/IkiWiki/Plugin/po.pm':
    source  => 'puppet:///modules/tails/weblate/patches/po.pm',
    mode    => '0755',
    owner   => root,
    group   => root,
    require => Package['ikiwiki'],
  }

  # The following file is a slightly patched version of the original one
  # (included in Debian Stretch), to address a memory leak that spits out
  # unharmful (but annoying) exceptions every once in a while. We need that to
  # avoid noise in our custom Python scripts.
  #
  # TODO: remove once the VM is upgraded to a newer version of Debian.

  ensure_packages(['libpython3.5-minimal'])

  file { '/usr/lib/python3.5/weakref.py':
    source  => 'puppet:///modules/tails/weblate/patches/weakref.py',
    mode    => '0644',
    owner   => root,
    group   => root,
    require => Package['libpython3.5-minimal'],
  }

}
