# Weblate Docker container
class tails::weblate::podman (
  String $weblate_admin_password,
  String $weblate_secret_key,
  String $postgres_password,
  String $redis_password,
  Array[String] $weblate_alternative_domains = [],
  String $weblate_admin_name = 'Weblate Admin',
  String $weblate_admin_email = 'tails-weblate@boum.org',
  String $weblate_email_host = '10.0.2.2',
  String $weblate_email_port = '25',
  String $weblate_server_email = 'tails-sysadmins@boum.org',
  String $weblate_default_from_email = 'weblate@translate.tails.boum.org',
  String $weblate_default_commiter_email = 'tails-l10n@boum.org',
  String $weblate_default_commiter_name = 'Tails translators',
  String $weblate_mt_tmserver = 'http://10.0.2.2:8888/tmserver/',
  String $postgres_host = '10.0.2.2',
  String $postgres_user = 'weblate',
  String $postgres_port = '5432',
  String $redis_host = '10.0.2.2',
  String $redis_port = '6379',
  String $redis_db = '1',
  Array[String] $extra_volumes = [],  # Used for testing purposes
) inherits tails::weblate::params {

  class { 'podman':
    manage_subuid            => true,
    # podman-docker is currently only available in Debian experimental repo
    podman_docker_pkg_ensure => 'absent',
  }

  # Application data needs to be accessible to:
  #
  #   1. the `weblate` host user that starts the container and is root inside
  #      of it; and
  #   2. the `weblate` container user used to run the application.
  #
  # To satisfy those, we do the following:
  #
  #   - Set the UID/GID of the (host) user/group `weblate` to a chosen value
  #     with enough slack for assigning a uid/gid namespace to it.
  #
  #   - Map a user namespace of subuids/subgids to the (host) `weblate` user to
  #     allow it to use a range of values inside the container.
  #
  #   - Set the owner of app data files/dirs to the UID that corresponds to the
  #     `weblate` user inside the container.
  #
  #   - Set the group of app data files/dirs to the GID that corresponds
  #     to the `weblate` group outside the container.
  #
  # This is an example of the mapping that we do:
  #
  #        | In the host                   | In the container     |
  #  , --- | ----------------------------- | -------------------- |
  #  | UID | 2001000 (in subuid namespace) | 1000 (weblate)       |
  #  | GID | 2000000 (weblate)             | 0    (root)          |
  #

  # Map a set of subuids and subgids to the selected user and group.

  $subuid_start = $system_uid + 1
  $subgid_start = $system_gid + 1

  podman::subuid { $system_user:
    subuid  => $subuid_start,
    count   => 65536,
    require => User[$system_user],
  }

  podman::subgid { $system_group:
    subgid  => $subgid_start,
    count   => 65536,
    require => Group[$system_group],
  }

  sysctl::value {
    'kernel.unprivileged_userns_clone': value => 1
  }

  ## Container configuration

  # Weblate config will, by default, try to grab a secret key from
  # `${weblate_data_dir}/secret` if that file exists, replacing the value
  # passed here through `$weblate_secret_key` in such case. Because of that, we
  # want to make sure that that file is absent.
  file { "${weblate_data_dir}/secret":
    ensure => absent,
  }

  file { "${weblate_config_dir}/podman.env":
    ensure  => present,
    content => epp('tails/weblate/podman.env.epp', {
      weblate_site_domain            => $weblate_site_domain,
      weblate_alternative_domains    => $weblate_alternative_domains,
      weblate_admin_name             => $weblate_admin_name,
      weblate_admin_password         => $weblate_admin_password,
      weblate_admin_email            => $weblate_admin_email,
      weblate_email_host             => $weblate_email_host,
      weblate_email_port             => $weblate_email_port,
      weblate_server_email           => $weblate_server_email,
      weblate_default_from_email     => $weblate_default_from_email,
      weblate_default_commiter_email => $weblate_default_commiter_email,
      weblate_default_commiter_name  => $weblate_default_commiter_name,
      weblate_secret_key             => $weblate_secret_key,
      weblate_mt_tmserver            => $weblate_mt_tmserver,
      postgres_database              => $postgres_database,
      postgres_host                  => $postgres_host,
      postgres_user                  => $postgres_user,
      postgres_password              => $postgres_password,
      postgres_port                  => $postgres_port,
      redis_host                     => $redis_host,
      redis_port                     => $redis_port,
      redis_db                       => $redis_db,
      redis_password                 => $redis_password,
    }),
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0640',  # contains passwords
    notify  => Podman::Container['weblate'],
    require => File[$weblate_config_dir]
  }

  # We need a patched settings file to fix https://github.com/WeblateOrg/weblate/issues/4037
  # XXX This needs to be updated each time we upgrade to a version < 4.1.1.
  # XXX Remove when upgrading to Weblate 4.1.1
  file { "${weblate_config_dir}/settings_docker.py":
    ensure  => present,
    source  => 'puppet:///modules/tails/weblate/config/settings_docker.py',
    owner   => $system_uid + 1000,  # Owner is `weblate` inside the container (UID 1000)
    group   => $system_gid,
    mode    => '0644',
    notify  => Podman::Container['weblate'],
    require => File[$weblate_config_dir]
  }

  # XXX Overlaying some libraries is needed because the libgnutls30 version in
  #     the Weblate 3.11.3 container is now unable to verify the current Let's
  #     Encrypt certificate used in our GitLab instance.
  # XXX Remove (from here and from `run_in_container.sh` once we runa newer
  #     container with an up-to-date versino of libgnutls30.
  $overlay_libraries = [
    '/usr/lib/x86_64-linux-gnu/libgnutls.so.30:/usr/lib/x86_64-linux-gnu/libgnutls.so.30:z',
    '/usr/lib/x86_64-linux-gnu/libhogweed.so.6:/usr/lib/x86_64-linux-gnu/libhogweed.so.6:z',
    '/usr/lib/x86_64-linux-gnu/libnettle.so.8:/usr/lib/x86_64-linux-gnu/libnettle.so.8:z',
    '/usr/lib/x86_64-linux-gnu/libffi.so.7:/usr/lib/x86_64-linux-gnu/libffi.so.7:z',
  ]

  podman::container { 'weblate':
    image   => "docker.io/weblate/weblate:${weblate_version}",
    user    => $system_user,
    flags   => {
      'publish'  => '8080:8080',
      'volume'   => [
        "${weblate_data_dir}:/app/data:z",
        "${weblate_config_dir}/settings_docker.py:/usr/local/lib/python3.7/dist-packages/weblate/settings_docker.py:Z",
      ] + $overlay_libraries + $extra_volumes,
      'env-file' => "${weblate_config_dir}/podman.env",
      # this container will be able to see the hosts localhost at 10.0.2.2
      'network'  => 'slirp4netns:allow_host_loopback=true',
    },
    require => [
      User[$system_user],
      Sysctl::Value['kernel.unprivileged_userns_clone'],
    ]
  }

  # We need Python's YAML module for some of our custom scripts that need to be
  # run inside the container. But the container itself doesn't include it, so
  # we link to a package installed on the host.
  #
  # XXX: remove this (and fix the run_in_container.sh script sccordingly) if
  #      ever (a) we stop depending on YAML or (b) a newer version of the
  #      Weblate container includes it.
  ensure_packages(['python3-yaml'])

  file { "${weblate_scripts_dir}/run_in_container.sh":
    ensure  => present,
    content => epp('tails/weblate/run_in_container.sh.epp', {
      weblate_home        => $weblate_home,
      weblate_config_dir  => $weblate_config_dir,
      weblate_data_dir    => $weblate_data_dir,
      weblate_scripts_dir => $weblate_scripts_dir,
      weblate_repos_dir   => $weblate_repos_dir,
      weblate_logs_dir    => $weblate_logs_dir,
      weblate_version     => $weblate_version,
    }),
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0755',
    require => [
      File[$weblate_data_dir],
      File[$weblate_scripts_dir],
      Package['python3-yaml'],
    ],
  }

}
