# Manage credentials for users who admin weblate
class tails::weblate::service_admins(
  Array[String] $service_admins,
) {

  ### Ensure service admins exist in the system

  $service_admins.each |String $name| {

    user::groups::manage_user { "${name}_in_weblate":
      user    => $name,
      group   => 'weblate',
      require => Group['weblate'],
    }

    user::groups::manage_user { "${name}_in_weblate_admin":
      user    => $name,
      group   => 'weblate_admin',
      require => Group['weblate_admin'],
    }

  }

  ### Configure sudo so service admins can admin the service

  sudo::conf { 'weblate-admin':
    source  => 'puppet:///modules/tails/weblate/sudo/weblate-admin',
    require => Group['weblate_admin'],
  }

  sudo::conf { 'puppeteers':
    source  => 'puppet:///modules/tails/weblate/sudo/puppeteers',
    require => Group['weblate_admin'],
  }

}
