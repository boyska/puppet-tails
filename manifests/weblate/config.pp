# Put Weblate configuration in place
class tails::weblate::config inherits tails::weblate::params {

  include tails::weblate::webserver

  ensure_packages(['memcached'])

  service { 'memcached':
    ensure  => running,
    require => Package[memcached],
  }

  file { $weblate_config_dir:
    ensure => directory,
    owner  => $system_uid,
    group  => $system_gid,
  }

}
