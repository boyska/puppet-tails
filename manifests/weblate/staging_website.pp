# A copy of the Tails website to preview translations
class tails::weblate::staging_website(
) inherits tails::weblate::params {

  include tails::website::builder

  # XXX The following variables are all used in the ikiwiki.setup template
  #     below, and should be updated whenever that file is updated.
  $src_dir = "${weblate_repos_dir}/staging/wiki/src"
  $git_dir = ''
  $web_dir = $staging_www_dir
  $url = 'https://staging.tails.boum.org'
  $cgi_url = "${url}/ikiwiki.cgi"
  $is_staging = true
  $underlays = []
  $po_slave_languages = $weblate_slave_languages

  file { $web_dir:
    ensure => directory,
    mode   => '0755',
    owner  => $system_uid,
    group  => $system_gid,
  }

  file { "${weblate_config_dir}/ikiwiki.setup":
    content => template('tails/website/ikiwiki.setup.erb'),
    mode    => '0644',
    owner   => weblate,
    group   => weblate,
  }

}
