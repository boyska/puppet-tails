# Manage hooks on puppet-tails.git
class tails::gitolite::hooks::puppet {

  file { '/var/lib/gitolite/repositories/puppet-tails.git/hooks/':
    ensure => directory,
    mode   => '0755',
    owner  => gitolite,
    group  => gitolite,
  }

  file { '/var/lib/gitolite/repositories/puppet-tails.git/hooks/pre-receive':
    source => 'puppet:///modules/tails/gitolite/hooks/puppet_sanity_check-pre-receive.hook',
    mode   => '0700',
    owner  => gitolite,
    group  => gitolite,
  }

  ensure_packages(['puppet','puppet-lint'])
}
