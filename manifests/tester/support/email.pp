# Manage services needed to provide a local email server that can be used
# by Thunderbird in our automated test suite.

class tails::tester::support::email (
  String $email_password,
  String $email_password_salt = $::fqdn,
  String $email_user          = "test@${::fqdn}",
) {

  ### Common resources

  $packages = [
    'dovecot-core',
    'dovecot-imapd',
    'dovecot-lmtpd',
    'dovecot-pop3d',
    'ssl-cert',
    'swaks',
  ]

  ensure_packages($packages)

  $hashed_email_password = pw_hash(
    $email_password, 'SHA-512', $email_password_salt
  )

  ### Constants

  $dovecot_ssl_cert      = '/etc/dovecot/private/dovecot.pem'
  $dovecot_ssl_key       = '/etc/dovecot/private/dovecot.key'
  $postfix_instance_name = 'TailsToaster'
  $postfix_conf_dir      = "/etc/postfix-${postfix_instance_name}"
  $postfix_service       = "postfix@postfix-${postfix_instance_name}"
  $postfix_lib_dir       = "/var/lib/postfix-${postfix_instance_name}"
  $postfix_spool_dir     = "/var/spool/postfix-${postfix_instance_name}"
  $postfix_tls_cert_file = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
  $postfix_tls_key_file  = '/etc/ssl/private/ssl-cert-snakeoil.key'

  ### Dovecot

  service { 'dovecot':
    ensure  => running,
    enable  => true,
    require => [
      File['/etc/dovecot/conf.d/99-tails-tester-support.conf'],
      File_line['dovecot_disable_auth-system'],
      Package['dovecot-imapd'],
      Package['dovecot-pop3d'],
      Package['ssl-cert'],
      User['vmail'],
    ],
  }

  user { 'vmail':
    uid        => '5000',
    home       => '/var/vmail',
    managehome => true,
  }

  group { 'vmail':
    gid        => '5000',
  }

  file { $dovecot_ssl_cert:
    ensure  => link,
    target  => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    require => Package['ssl-cert'],
  }

  file { $dovecot_ssl_key:
    ensure  => link,
    target  => '/etc/ssl/private/ssl-cert-snakeoil.key',
    require => Package['ssl-cert'],
  }

  file { '/etc/dovecot/conf.d/99-tails-tester-support.conf':
    content => template('tails/tester/support/email/dovecot/tails-tester-support.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      File[$dovecot_ssl_cert],
      File[$dovecot_ssl_key],
      Package['dovecot-core'],
    ],
    notify  => Service['dovecot'],
  }

  file { '/etc/dovecot/passwd':
    content => "${email_user}:${hashed_email_password}::::::\n",
    owner   => 'root',
    group   => 'dovecot',
    mode    => '0640',
  }

  file_line { 'dovecot_disable_auth-system':
    path   => '/etc/dovecot/conf.d/10-auth.conf',
    line   => '#!include auth-system.conf.ext',
    match  => '#?!include auth\-system\.conf\.ext',
    notify => Service['dovecot'],
  }

  ### Postfix

  # XXX: once a PoC is ready, consider refactoring to de-duplicate
  # stuff copied/adapted from tails::whisperback::relay; this should
  # not be too hard: everything below was paramaterized with
  # $postfix_instance_name and config files were turned into templates.

  postfix::config {
    'multi_instance_wrapper':
      # lint:ignore:single_quote_string_with_variables -- Postfix variable
      value => '${command_directory}/postmulti -p --}';
      # lint:endignore
    'multi_instance_enable':
      value => 'yes';
    'multi_instance_directories':
      value   => $postfix_conf_dir,
      require => File[$postfix_conf_dir];
  }

  service { $postfix_service:
    ensure  => running,
    enable  => true,
    require => [
      Service['postfix'],
      Postfix::Config['multi_instance_directories'],
      File["${postfix_conf_dir}/dynamicmaps.cf"],
      Exec["Seed /etc in chroot for ${postfix_instance_name}"],
    ],
  }

  file { [$postfix_conf_dir, $postfix_spool_dir]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755';
  }

  file { $postfix_lib_dir:
    ensure => directory,
    owner  => 'postfix',
    group  => 'postfix',
    mode   => '0755',
  }

  file { "${postfix_conf_dir}/master.cf":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('tails/tester/support/email/postfix/master.cf.erb'),
    notify  => Service[$postfix_service],
    require => Package['postfix'],
  }

  file { "${postfix_conf_dir}/main.cf":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('tails/tester/support/email/postfix/main.cf.erb'),
    notify  => Service[$postfix_service],
    require => [
      Package['postfix'],
      File[$postfix_lib_dir],
      File[$postfix_spool_dir],
    ],
  }

  file { "${postfix_conf_dir}/dynamicmaps.cf":
    ensure => link,
    target => '/etc/postfix/dynamicmaps.cf',
    notify => Service[$postfix_service],
  }

  file { "${postfix_conf_dir}/vmailbox":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "${email_user}  whatever",
    notify  => Exec["Compile vmailbox for ${postfix_instance_name}"],
    require => Package['postfix'],
  }

  exec { "Seed /etc in chroot for ${postfix_instance_name}":
    command => "cp -a /var/spool/postfix/etc ${postfix_spool_dir}/etc",
    onlyif  => "test ! -d ${postfix_spool_dir}/etc",
    require => [
      File[$postfix_spool_dir],
      Service['postfix'],
    ],
  }

  exec { "Compile vmailbox for ${postfix_instance_name}":
    command     => "postmap '${postfix_conf_dir}/vmailbox'",
    refreshonly => true,
    notify      => Service[$postfix_service],
  }

  ### nginx

  class { '::nginx':
    access_log => 'noip',
    require    => Tails::Dhparam['/etc/nginx/dhparams.pem'],
  }
  tails::dhparam { '/etc/nginx/dhparams.pem': }

  file { '/etc/nginx/sites-enabled/default':
    ensure  => absent,
    require => Package[nginx],
  }

  nginx::vhostsd { $::fqdn:
    content => template('tails/tester/support/email/nginx/vhost.erb'),
    notify  => Service[nginx],
    require => Package[nginx],
  }

  file { [
    '/var/www/html/.well-known',
    '/var/www/html/.well-known/autoconfig',
    '/var/www/html/.well-known/autoconfig/mail',
  ]:
    ensure  => directory,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0750',
    require => Package['nginx'],
  }

  file { '/var/www/html/.well-known/autoconfig/mail/config-v1.1.xml':
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => template('tails/tester/support/email/nginx/autoconfig.xml.erb')
  }

}
