# Manage systemd-timesyncd

class tails::timesyncd (
  Enum['present', 'absent'] $ensure = 'present',
  Array[String] $ntp_servers        = [],
) {

  $service_ensure = $ensure ? {
    absent  => stopped,
    default => running,
  }

  $service_enable = $ensure ? {
    absent  => false,
    default => true,
  }

  service { 'systemd-timesyncd.service':
    ensure   => $service_ensure,
    enable   => $service_enable,
    provider => systemd,
  }

  file { '/etc/systemd/timesyncd.conf.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { '/etc/systemd/timesyncd.conf.d/puppet-tails.conf':
    ensure => $ensure,
    owner  => root,
    group  => root,
    mode   => '0644',
  }

  ini_setting { 'timesyncd NTP servers':
    ensure  => $ensure,
    path    => '/etc/systemd/timesyncd.conf.d/puppet-tails.conf',
    section => 'Time',
    setting => 'NTP',
    value   => join($ntp_servers, ' '),
    notify  => Service['systemd-timesyncd.service'],
    require => File['/etc/systemd/timesyncd.conf.d'],
  }

}
