# profile to ensure a node has hiera-eyaml installed and keys generated
class tails::profile::eyaml (
  String $puppetuser = 'puppet',
) {

  ensure_packages(['hiera-eyaml'])

  file { '/etc/puppet/keys':
    ensure => directory,
    mode   => '0755',
    owner  => $puppetuser,
  }

  exec { '/usr/bin/eyaml createkeys':
    user    => $puppetuser,
    cwd     => '/etc/puppet',
    creates => '/etc/puppet/keys/private_key.pkcs7.pem',
    require => [ File['/etc/puppet/keys'] , Package['hiera-eyaml'] ],
  }

}
