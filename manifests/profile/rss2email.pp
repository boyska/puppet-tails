# Receive RSS feeds by e-mail
#
# Can be configured through Hiera by setting the following hash:
#
#   tails::profile::rss2email::feeds:
#     feedname:
#       url: https://groups.google.com/forum/feed/groupname/msgs/rss.xml?num=50
#       from: feeds@lizard.tails.boum.org
#       to: tails-sysadmins@boum.org
#
class tails::profile::rss2email (
  String $user                      = 'rss2email',
  String $default_from              = 'feeds@lizard.tails.boum.org',
  String $default_to                = 'tails-syadmins@boum.org',
  Hash   $feeds                     = {},
  Stdlib::Absolutepath $homedir     = "/var/lib/${user}",
  Stdlib::Absolutepath $config_file = "${homedir}/.config/rss2email.cfg",
  Stdlib::Absolutepath $database    = "${homedir}/.local/share/rss2email.json",
){

  ensure_packages(['rss2email', 'jq'])

  user { $user:
    ensure     => present,
    home       => $homedir,
    managehome => true,
    system     => true,
  }

  # initialize unless config file is already present
  exec { 'r2e new':
    command => "/usr/bin/r2e new ${default_to}",
    creates => "${homedir}/.config/rss2email.cfg",
    user    => $user,
    require => User[$user]
  }

  $feeds.each | String $feedname, Hash $config | {

    $from    = $config['from']    ? { undef => $default_from, default => $config['from']    }
    $to      = $config['to']      ? { undef => $default_to,   default => $config['to']      }
    $filters = $config['filters'] ? { undef => {},            default => $config['filters'] }

    tails::profile::rss2email::feed { $feedname:
      url     => $config['url'],
      from    => $from,
      to      => $to,
      filters => $filters,
    }

  }

  cron { 'r2e run':
    command => '/usr/bin/r2e run',
    user    => $user,
    hour    => '*',
    minute  => '*/30',
  }

  #
  # Feed filters -- Use with tails::profile::rss2email::filter
  #

  $python_path = '/usr/local/lib/python3/dist-packages'

  file { [
      '/usr/local/lib',
      '/usr/local/lib/python3',
      $python_path,
    ]:
    ensure => directory,
    owner  => root,
    group  => staff,
    mode   => '2755',
  }

  file { "${python_path}/feedfilters.py":
    source  => 'puppet:///modules/tails/rss2email/feedfilters.py',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$python_path],
  }

}
