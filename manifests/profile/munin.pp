# Configure a libvirt host to allow Riseup's Munin service to connect and grab
# resource usage information.
class tails::profile::munin {

  # XXX The 'diskstats' plugin from Stretch repository doesn't work with our
  # current kernel, so we pull 'munin*' form backports. This pinning will stop
  # being applied once lizard is upgraded to a newer Debian, so it can/should
  # be removed once upgrade happens.
  if $::facts['os']['release']['major'] == '9' {
    apt::pin { 'munin':
      packages   => 'munin*',
      originator => 'Debian Backports',
      codename   => 'stretch-backports',
      priority   => 991,
    }
  }

  # XXX The 'diskstats' plugin from Buster repository doesn't work with our
  # current kernel, so we pull 'munin*' form backports. This pinning will stop
  # being applied once iguana is upgraded to a newer Debian, so it can/should
  # be removed once upgrade happens.
  if $::facts['os']['release']['major'] == '10' {
    apt::pin { 'munin':
      packages   => 'munin*',
      originator => 'Debian Backports',
      codename   => 'buster-backports',
      priority   => 991,
    }
  }

  class { '::munin':
    allow            => [ '198.252.153.17' ],
    manage_shorewall => true,
  }

  package { 'munin-libvirt-plugins':
    ensure => present
  }

  munin::plugin {
    [
      'libvirt-blkstat',
      'libvirt-mem',
      'libvirt-cputime',
      'libvirt-ifstat'
    ]:
    require => Package['munin-libvirt-plugins'],
  }

  # Start munin-node after libvirtd.service, otherwise plugins such as
  # libvirt-blkstat don't autoconfigure properly.
  file { '/etc/systemd/system/munin-node.service.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }
  file { '/etc/systemd/system/munin-node.service.d/after-libvirtd.conf':
    content => "[Unit]\nAfter=libvirtd.service\n",
  }

}
