# fixing facts for nodes running on stretch

class tails::profile::stretch {

  file { '/etc/facter':
    ensure => directory,
  }

  file { '/etc/facter/facts.d':
    ensure => directory,
  }

  file { '/etc/facter/facts.d/facts.yaml':
    ensure  => present,
    content => template('tails/profile/stretch/stretchfacts.yaml.erb'),
  }

}
