# profile for our monitoring satellites
class tails::profile::ssh (
  String $sshd_tcp_forwarding = 'no',
  String $sshd_x11_forwarding = 'no',
  Hash $sshkeys               = {},
) {

# install ssh daemon

  ensure_packages( [ 'openssh-server' ] )

# set up the service

  service { 'ssh':
    ensure => running,
    enable => true,
  }

# configure sshd

  file { '/etc/ssh/sshd_config':
    content => template('tails/profile/ssh/sshd_config.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['openssh-server'],
    notify  => Service['ssh'],
  }

# knoweth thy neighbour, a.k.a. fill our known_hosts file

  ensure_resources('sshkey',$sshkeys)

}
