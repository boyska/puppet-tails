# profile for puppetmaster

class tails::profile::puppetmaster (
  Array $puppeteers = [],
  Hash $users       = lookup(users,undef,undef,{}),
) {

  file { [
    '/opt',
    '/opt/puppetlabs',
    '/opt/puppetlabs/server',
    '/opt/puppetlabs/server/apps',
    '/opt/puppetlabs/server/apps/puppetserver',
    '/etc/puppet/conf.d',
  ]:
    ensure => directory,
  }

# dirty hack to prevent the puppetmodule from resetting vardir ownership
# and restarting puppet master all the time

  exec { '/bin/chown root:root /var/cache/puppet': }

  class { 'puppet':
    manage_packages            => false,
    agent                      => true,
    runmode                    => 'systemd.timer',
    server                     => true,
    server_reports             => 'store,puppetdb',
    server_foreman             => false,
    server_external_nodes      => '',
    server_git_repo            => true,
    server_git_repo_path       => '/var/lib/puppet/puppet-code.git',
    server_storeconfigs        => true,
    server_puppetserver_dir    => '/etc/puppet',
    server_puppetserver_vardir => '/var/lib/puppet/server_data',
    server_puppetserver_rundir => '/var/run/puppet',
    server_puppetserver_logdir => '/var/log/puppet',
    ssldir                     => '/var/lib/puppet/ssl',
    vardir                     => '/var/cache/puppet',
    sharedir                   => '/usr/share/puppet',
    hiera_config               => '/etc/puppet/code/environments/production/hiera.yaml',
    require                    => Exec['/bin/chown root:root /var/cache/puppet'],
  }

  class { 'puppet::server::puppetdb':
    server => $::fqdn,
  }

  include tails::profile::eyaml

  $puppeteers.each | String $puppeteer | {
    ssh_authorized_key { "${puppeteer}-to-code-repo":
      ensure => present,
      user   => puppet,
      type   => $users[$puppeteer][sshkeys][personal]['type'],
      key    => $users[$puppeteer][sshkeys][personal][key],
    }
  }

# Clean up reports

  exec { '/usr/bin/find /var/lib/puppet/server_data/reports -type f -ctime +2 -exec /bin/rm {} \;': }

}
