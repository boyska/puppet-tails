# profile for isotesters, sofar only implementing the oneshot puppet run

class tails::profile::isotester {

  systemd::unit_file { 'puppet-oneshot.service':
    source => "puppet:///modules/tails/profile/isotester/puppet-oneshot.service",
    enable => true,
  }

}
