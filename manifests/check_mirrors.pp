# Manage checking of Tails mirrors
class tails::check_mirrors (
  String $email_recipient,
  Stdlib::Absolutepath $homedir        = '/var/lib/tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $user         = 'tails_check_mirrors',
  Pattern[/\A[a-z_]+\z/] $repo_user    = 'git',
  Stdlib::Host $repo_host              = 'gitlab-ssh.tails.boum.org',
  Pattern[/\A[a-z\/_-]+\z/] $repo_name = 'tails/check-mirrors',
  String $repo_rev                     = 'master',
  String $repo_ensure                  = 'latest',
  String $cronjob_script               = '/usr/local/bin/check_mirrors_cronjob.sh',
  String $cronjob_timeout              = '300m',
) {

  validate_email_address($email_recipient)

  $repo_checkout    = "${homedir}/check-mirrors"
  $needed_packages  = ['curl', 'ruby-nokogiri', 'wget']
  $gnupg_homedir    = "${homedir}/.gnupg"
  $signing_key_file = "${homedir}/tails-signing.key"

  package { $needed_packages: ensure => present }

  user { $user:
    ensure     => present,
    home       => $homedir,
    managehome => true,
    system     => true,
  }

  postfix::mailalias { $user:
    recipient => $email_recipient,
  }

  exec { "SSH key pair for user ${user}":
    command => "ssh-keygen -t rsa -b 4096 -N '' -f \"${homedir}/.ssh/id_rsa\"",
    user    => $user,
    creates => "${homedir}/.ssh/id_rsa",
  }

  file { $signing_key_file:
    owner  => $user,
    group  => $user,
    mode   => '0600',
    source => 'puppet:///modules/tails/check_mirrors/tails-signing.key',
  }

  file { $gnupg_homedir:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  exec { 'Import Tails signing key':
    user        => $user,
    group       => $user,
    command     => "gpg --batch --quiet --import '${signing_key_file}'",
    environment => "HOME=${homedir}",
    require     => File[$gnupg_homedir, $signing_key_file],
    subscribe   => File[$signing_key_file],
    refreshonly => true,
  }

  vcsrepo { $repo_checkout:
    ensure   => $repo_ensure,
    provider => git,
    source   => "${repo_user}@${repo_host}:${repo_name}.git",
    revision => $repo_rev,
    user     => $user,
    require  => [
      User[$user], Sshkey[$repo_host],
      Exec["SSH key pair for user ${user}"]
    ],
  }

  file { $cronjob_script:
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0755',
    content => epp('tails/check_mirrors/cronjob.sh.epp', {
      command         => "${repo_checkout}/check-mirrors.rb",
      cronjob_script  => $cronjob_script,
      cronjob_timeout => $cronjob_timeout,
    }),
  }

  tails::check_mirrors::cronjob { 'Full run':
    args   => ['--ignore-failures', 'failures.json', '--minimum-speed', '"7 MB/s"'],
    hour   => 0,
    minute => 16,
  }

  tails::check_mirrors::cronjob { 'Fast run #1: optimized for European morning':
    args   => ['--fast', '--store-failures', 'failures.json'],
    hour   => 4,
    minute => 16,
  }

  tails::check_mirrors::cronjob { 'Fast run #2: optimized for American morning':
    args   => ['--fast', '--store-failures', 'failures.json'],
    hour   => 12,
    minute => 16,
  }

  tails::check_mirrors::cronjob { 'Fast run #3: to complete the 3 × 8 shifts':
    args   => ['--fast', '--store-failures', 'failures.json'],
    hour   => 20,
    minute => 16,
  }

}
