# Manage a Weblate service
class tails::weblate (
  Array[String] $service_admins,
  String $postgres_password,
  String $weblate_admin_password,
  String $weblate_secret_key,
  String $redis_password,
  Array[String] $podman_extra_volumes = [],  # Used for testing purposes.
) inherits tails::weblate::params {

  ### Sanity checks

  if $::lsbdistcodename != 'bullseye' {
    fail('The tails::weblate class only supports Debian Bullseye.')
  }

  ### Operating system configurations

  include tails::weblate::users_and_groups

  class {'tails::weblate::service_admins':
    service_admins => $service_admins,
  }

  ensure_packages(['grep'])

  ### Weblate configuration

  file { $weblate_data_dir:
    ensure => directory,
    owner  => $ns_weblate_uid,
    group  => $ns_root_gid,
    mode   => '2755',
  }

  include tails::weblate::webserver

  class { 'tails::weblate::database':
    postgres_password => $postgres_password,
  }

  class { 'tails::weblate::redis':
    redis_password => $redis_password,
  }

  include tails::weblate::repositories
  Class['tails::weblate::users_and_groups'] -> Class['tails::weblate::repositories']

  include tails::weblate::config
  include tails::weblate::logs
  include tails::weblate::tmserver

  class { 'tails::weblate::podman':
    redis_password         => $redis_password,
    postgres_password      => $postgres_password,
    weblate_admin_password => $weblate_admin_password,
    weblate_secret_key     => $weblate_secret_key,
    extra_volumes          => $podman_extra_volumes,
    require                => [
      Class['tails::weblate::users_and_groups'],
      Class['tails::weblate::repositories'],
    ]
  }

  ### Integration with Tails website

  include tails::weblate::scripts
  include tails::weblate::staging_website

}
