# Manage a SMTP relay used by WhisperBack to send email to Tails help desk
class tails::whisperback::relay (
  Optional[String] $always_bcc = undef,
) {

  package { 'swaks': ensure => installed }

  augeas { 'loopback_interface' :
    context => '/files/etc/network/interfaces',
    changes =>
    [
      "set auto[child::1 = 'lo']/1 lo",
      "set iface[. = 'lo'] lo",
      "set iface[. = 'lo']/family inet",
      "set iface[. = 'lo']/method loopback",
      "set iface[. = 'lo']/up 'ip addr add 127.0.0.2/32 dev lo'",
    ],
  }

  $postfix_instance_conf_dir = '/etc/postfix-hidden'

  include tor::daemon
  tor::daemon::onion_service { 'tails_whisperback_relay':
    ports      => [ '25' ],
    require    => Service['postfix@postfix-hidden'],
  }

  file { '/var/lib/tor/tails_whisperback_relay/hs_ed25519_secret_key':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => Tor::Daemon::Onion_service['tails_whisperback_relay'],
    source  => 'puppet:///modules/tails_secrets_whisperback/tor/hs_ed25519_secret_key',
  }

  file { '/var/lib/tor/tails_whisperback_relay/hs_ed25519_public_key':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => File['/var/lib/tor/tails_whisperback_relay/hs_ed25519_secret_key'],
    source  => 'puppet:///modules/tails_secrets_whisperback/tor/hs_ed25519_public_key',
  }

  file { '/var/lib/tor/tails_whisperback_relay/hostname':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => File['/var/lib/tor/tails_whisperback_relay/hs_ed25519_public_key'],
    source  => 'puppet:///modules/tails_secrets_whisperback/tor/hostname',
    notify  => Service['tor'],
  }

  postfix::config {
    'multi_instance_wrapper':
      # lint:ignore:single_quote_string_with_variables -- Postfix variable
      value => '${command_directory}/postmulti -p --}';
      # lint:endignore
    'multi_instance_enable':
      value => 'yes';
    'multi_instance_directories':
      value   => $postfix_instance_conf_dir,
      require => File[$postfix_instance_conf_dir];
  }

  service { 'postfix@postfix-hidden':
    ensure    => running,
    enable    => true,
    subscribe => Postfix::Hash['/etc/postfix/tls_policy'],
    require   => [
      Service['postfix'],
      Postfix::Config['multi_instance_directories'],
      File["${postfix_instance_conf_dir}/dynamicmaps.cf"],
    ],
  }

  file { $postfix_instance_conf_dir:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  postfix::hash { "${postfix_instance_conf_dir}/access":
    source  => 'puppet:///modules/tails/whisperback/relay/access',
    require => File[$postfix_instance_conf_dir],
  }

  file { "${postfix_instance_conf_dir}/aliases":
    content => template('tails/whisperback/relay/aliases.erb'),
    require => File[$postfix_instance_conf_dir],
    notify  => Exec["generate ${postfix_instance_conf_dir}/aliases.db"],
  }
  file { "${postfix_instance_conf_dir}/aliases.db":
    ensure  => present,
    require => [
      File["${postfix_instance_conf_dir}/aliases"],
      Exec["generate ${postfix_instance_conf_dir}/aliases.db"]
    ],
  }
  exec { "generate ${postfix_instance_conf_dir}/aliases.db":
    command     => "postalias ${postfix_instance_conf_dir}/aliases",
    refreshonly => true,
    require     => Package['postfix'],
    subscribe   => File["${postfix_instance_conf_dir}/aliases"],
  }

  file {
    "${postfix_instance_conf_dir}/master.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/tails/whisperback/relay/master.cf',
      notify  => Service['postfix@postfix-hidden'],
      require => Package['postfix'];

    "${postfix_instance_conf_dir}/main.cf":
      ensure  => present,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('tails/whisperback/relay/main.cf.erb'),
      notify  => Service['postfix@postfix-hidden'],
      require => [
        Package['postfix'],
        File['/var/lib/postfix-hidden'],
        File['/var/spool/postfix-hidden'],
        File["${postfix_instance_conf_dir}/access.db"],
        File["${postfix_instance_conf_dir}/aliases.db"],
      ];
    '/var/lib/postfix-hidden':
      ensure => directory,
      owner  => 'postfix',
      group  => 'postfix',
      mode   => '0755';
    '/var/spool/postfix-hidden':
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    "${postfix_instance_conf_dir}/dynamicmaps.cf":
      ensure => link,
      target => '/etc/postfix/dynamicmaps.cf',
      notify => Service['postfix@postfix-hidden'];
  }

  # XXX: is this still needed, now that instanciated postfix@.service
  # has ExecStartPre=/usr/lib/postfix/configure-instance.sh?
  exec { 'copy_postfix_chroot':
    command => 'cp -a /var/spool/postfix/etc /var/spool/postfix-hidden/etc',
    onlyif  => 'test ! -d /var/spool/postfix-hidden/etc',
    require => File['/var/spool/postfix-hidden'],
  }

}
