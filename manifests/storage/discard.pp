# Enable discard (aka. TRIM) operations (#11788)
class tails::storage::discard () {
  augeas { 'lvm.conf_issue_discards':
    context => '/files/etc/lvm/lvm.conf',
    changes => 'set devices/dict/issue_discards/int 1',
    require => Package[lvm2],
  }
  $fstrim_unit_types = ['service', 'timer']
  $fstrim_local_unit_ensure = $tails::apt::codename ? {
    'stretch' => present,
    # On Buster and newer, the fstrim units are installed under
    # /lib/systemd/system by the util-linux package, so we don't need
    # to copy them from /usr/share/doc/util-linux/examples/.
    default   => absent,
  }
  $fstrim_unit_types.each |String $unit_type| {
    file { "/etc/systemd/system/fstrim.${unit_type}":
      ensure => $fstrim_local_unit_ensure,
    }
    if $tails::apt::codename == 'stretch' {
      File["/etc/systemd/system/fstrim.${unit_type}"] {
        source => "/usr/share/doc/util-linux/examples/fstrim.${unit_type}",
      }
    }
  }
  service { 'fstrim.timer':
    ensure   => running,
    enable   => true,
    provider => systemd,
    require  => $fstrim_unit_types.map |String $unit_type| {
      File["/etc/systemd/system/fstrim.${unit_type}"]
    },
  }
}
