define tails::pdns::zone (
  Array $nameservers,
  Boolean $is_master,
  Stdlib::Fqdn $zone = $title,
) {

  include tails::pdns

  exec { "/usr/bin/pdnsutil create-zone ${zone}":
    unless => "/usr/bin/host -t soa ${zone} localhost",
  }

  exec { "/usr/bin/pdnsutil set-meta ${zone} SOA-EDIT-API INCEPTION-INCREMENTAL":
    unless  => "/usr/bin/pdnsutil get-meta ${zone} |grep -x 'SOA-EDIT-API = INCEPTION-INCREMENTAL'",
    onlyif  => "/usr/bin/host -t ns ${zone} localhost",
    require => Exec["/usr/bin/pdnsutil create-zone ${zone}"],
  }

  $nameservers.each |Stdlib::Fqdn $ns| {
    exec { "/usr/bin/pdnsutil add-record ${zone} @ NS 3600 ${ns}":
      unless  => "/usr/bin/host -t ns ${zone} localhost",
      require => Exec["/usr/bin/pdnsutil create-zone ${zone}"],
    }
  }

}
