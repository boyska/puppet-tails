# Manage our meeting script

class tails::meeting (
  Pattern[/\A[a-z_]+\z/] $user = 'tails_meeting_reminder',
) {
  $homedir = "/var/lib/${user}"
  $script_path = '/usr/local/bin/meeting.py'

  user { $user:
    ensure     => present,
    home       => $homedir,
    managehome => true,
    system     => true,
    require    => File['/usr/local/bin/meeting.py'],
  }

  file { $script_path:
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/meeting/meeting.py',
  }

  package { 'python3-requests': ensure => installed }

}
