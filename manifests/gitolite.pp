# Wrapper around the gitolite class with Tails-specific functionality and stuff
class tails::gitolite (
  Boolean $with_git_annex           = false,
  String $git_annex_with_recommends = 'default',
  String $ssh_pubkey_name           = 'gitolite@puppet-git.lizard',
) {

  class { '::gitolite':
    admin_key         => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+mdQZg1zWYTOwYvs9Anhn+SIAVsg47gj/nGdjVTZu9hehX9GCeU40OwI0qKIASpegweELwHjuRZ5Fhx9UGKYlWKa+fT/xpYg7wGeXM65N4KmzcppyYHjErerGPNbA4glAsmUmmv/IG69hgHVhezclu64lj6KeIYvyD6kEv1mu4yts584tLOip1Sig3XS4dyB5BRqDhD60CN5qQ55kYCLRxw6QfResl+Dqw6GDa8FOuHik0IESGaRAHqVeR+NC+xo2jMj05yi2WclLMIuV6orM9NI3WxCyHDpGXKeT5huR1OJeCW9mzkq0Z1bFswioX2NJsYO9VOunSsmp+XQkOw3y8mTYToCi8DWlAJ3yeu7aB51ofwgDfBY+gyyW/X8wrqzCTJ9VMScgfBm/4GLaJns8hYxtsBVYT6P+syC7kqVs+v2cr2pHQpleLb50MRVnuFPTILOdRMME7SOPqIuwlDDes1ItVHm5oR177q5BI9ywC2Anu/S26i+tE+8g0080voNNS5Pykbhz+o4S8DuO2pA0+ijzYB4t3Ic6bcUcC8RE7G5LG5V3Go8/pewvZUEXlGn7H7ti3raPjMZY5crrrU4rS851TD/wuu6kZdU1k3IFrxUFW5wt4IKS6sc5tZw8HO5jZc4WjZLbceoqiGW5TuTmORjLxseGbk7X8sMEjP9rpQ== intrigeri@ensifera', # lint:ignore:140chars -- SSH key
    gituser           => gitolite3,
    package           => gitolite3,
    version           => 3,
    deploy_gitoliterc => false,
    backward_compat   => true,
  }
  file { '/etc/systemd/system/git-daemon.service':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => '[Unit]
Description=Git Daemon Instance

[Service]
User=gitolite3
Group=gitolite3
ExecStart=/usr/lib/git-core/git-daemon --reuseaddr --base-path=/var/lib/gitolite3/repositories /var/lib/gitolite3/repositories

[Install]
WantedBy=multi-user.target
',
  }

  service { 'git-daemon':
    ensure    => running,
    provider  => systemd,
    require   => [
      Package['git'],
      File['/etc/systemd/system/git-daemon.service']
    ],
    subscribe => File['/etc/systemd/system/git-daemon.service'],
  }
  exec { 'systemctl enable git-daemon.service':
    creates => '/etc/systemd/system/multi-user.target.wants/git-daemon.service',
  }

  # Mirroring support
  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => gitolite,
    home    => '/var/lib/gitolite3',
  }
  sshkeys::set_client_key_pair { 'gitolite@puppet-git.lizard_role-weblate-gatekeeper':
    user     => gitolite,
    home     => '/var/lib/gitolite3',
    filename => 'id_rsa_role-weblate-gatekeeper',
  }
  file { '/var/lib/gitolite3/.ssh/config':
    source => 'puppet:///modules/tails/gitolite/ssh/config',
    owner  => gitolite3,
    group  => gitolite3,
    mode   => '0600',
  }

  # git-annex support
  if $with_git_annex == true {

    file { '/etc/gitolite3/gitolite.rc':
      source => 'puppet:///modules/tails/gitolite/gitolite-with-annex-shell.rc',
      owner  => root,
      group  => root,
      mode   => '0644',
    }

    class { '::tails::git_annex':
      with_recommends => $git_annex_with_recommends,
    }

  }

  class { '::tails::gitolite::weblate_gatekeeper': }
  class { '::tails::gitolite::hooks::common': }
  class { '::tails::gitolite::hooks::jenkins_jobs': }
  class { '::tails::gitolite::hooks::tails': }
  class { '::tails::gitolite::hooks::puppet': }

}
