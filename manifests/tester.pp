# Set up what's necessary to run automated tests
class tails::tester (
  Boolean $manage_temp_dir_mount            = false,
  String $temp_dir                          = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^(buster|bullseye)$/ {
    warning('The tails::tester class only supports Debian Buster and Bullseye.')
  }

  ### Resources

  $buster_and_older_only_ensure = $::lsbdistcodename ? {
    buster  => present,
    default => absent,
  }

  class { '::libvirt::host':
    qemu_security_driver => 'none',
  }

  apt::pin { 'qemu':
    ensure     => $buster_and_older_only_ensure,
    packages   => ['qemu*', 'libfdt1'],
    originator => 'Debian Backports',
    codename   => 'buster-backports',
    priority   => 991,
  }

  apt::pin { 'tor':
    ensure     => $buster_and_older_only_ensure,
    packages   => ['tor', 'tor-geoipdb'],
    originator => 'Debian Backports',
    codename   => 'buster-backports',
    priority   => 991,
  }

  $tester_packages = [
    'cucumber',
    'devscripts',
    'dnsmasq-base',
    'ffmpeg',
    'gawk',
    'git',
    'i18nspector',
    'imagemagick',
    'libcap2-bin',
    'libvirt0',
    'libvirt-clients',
    'libvirt-daemon-system',
    'libvirt-dev',
    'obfs4proxy',
    'ovmf',
    'pry',
    'python3-opencv',
    'python3-pil',
    'python3-potr',
    'python3-slixmpp',
    'qemu-system-x86',
    'redir',
    'ruby-guestfs',
    'ruby-json',
    'ruby-libvirt',
    'ruby-net-irc',
    'ruby-packetfu',
    'ruby-rb-inotify',
    'ruby-rjb',
    'ruby-rspec',
    'ruby-test-unit',
    'seabios',
    'tcpdump',
    'tcplay',
    'unclutter',
    'virt-viewer',
    'x11vnc',
    'x264',
    'xdotool',
    'xtightvncviewer',
    'xvfb',
  ]

  ensure_packages($tester_packages)

  file { '/etc/tmpfiles.d/TailsToaster.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "d  ${temp_dir}  0755  root  root  -\n",
  }

  if $manage_temp_dir_mount {
    validate_string(
      $temp_dir_backing_device,
      $temp_dir_fs_type,
      $temp_dir_mount_options
    )

    mount { $temp_dir:
      ensure  => mounted,
      device  => $temp_dir_backing_device,
      fstype  => $temp_dir_fs_type,
      options => $temp_dir_mount_options,
      require => File['/etc/tmpfiles.d/TailsToaster.conf']
    }

  }
}
