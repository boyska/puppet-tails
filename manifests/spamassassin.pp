# Manage SpamAssassin
class tails::spamassassin () {

  class { 'spamassassin':
    configdir         => '/etc/spamassassin',
    sa_update         => true,
    run_execs_as_user => 'amavis',
    service_enabled   => false,
    pyzor_enabled     => false,
    razor_enabled     => false,
    spamcop_enabled   => false,
  }

}
