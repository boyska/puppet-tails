# Manage AMaViSd-new
class tails::amavisd_new (
  Array[String] $local_domains = [],
) {

  package { 'amavisd-new':
    ensure => installed,
  }

  service { 'amavis':
    ensure  => running,
    enable  => true,
    require => Package['amavisd-new'],
  }

  file { '/etc/amavis/conf.d/99-custom':
    content => template('tails/amavisd_new/99-custom.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      Package['amavisd-new'],
      Package['spamassassin'],
    ],
    notify  => Service['amavis'],
  }

}
