# Manage a rsync server
class tails::rsync (
  Array[String] $auth_users, # array of user names
  Array[String] $release_managers, # array of user names
  Stdlib::Absolutepath $basedir = '/srv/rsync',
  # Used by release managers during the release process
  Stdlib::Absolutepath $tmpdir  = '/srv/tmp',
  String $max_connections       = '23',
  String $secrets_source        = 'puppet:///modules/tails_secrets_rsync/users/tails.secrets',
  ) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $data_dir    = "${basedir}/tails"
  $secrets_dir = '/etc/rsync.d'
  $secrets     = "${secrets_dir}/tails.secrets"

  ### Resources

  package { 'acl': ensure => present }
  ensure_packages(['rsync'])

  user { 'rsync_tails':
    ensure     => present,
    system     => true,
    home       => '/home/rsync_tails',
    managehome => true,
  }

  file { [ $basedir, $secrets_dir ]:
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { [$data_dir, $tmpdir]:
    ensure  => directory,
    owner   => root,
    group   => rsync_tails,
    mode    => '2775',
    require => [ File[$basedir], User['rsync_tails'] ],
    notify  => Exec['set-acl-on-rsync-data-dir', 'set-acl-on-rsync-tmp-dir'],
  }

  file { '/etc/rsyncd.conf':
    content => template('tails/rsync/rsyncd.conf.erb'),
    owner   => root,
    group   => root,
    mode    => '0600',
    notify  => Service[rsync],
  }

  file { $secrets:
    source  => $secrets_source,
    owner   => root,
    group   => 'rsync_tails',
    mode    => '0640',
    require => User['rsync_tails'],
  }

  service { 'rsync':
    ensure  => running,
    require => Package[rsync],
  }

  $acls = "${common::moduledir::module_dir_path}/tails/rsync-dir.acl"
  file { $acls:
    content => "group::rwX\ndefault:group::rwX\nother::r-X\ndefault:other::r-X\n",
    mode    => '0600',
    owner   => root,
    group   => root,
  }
  exec {
    'set-acl-on-rsync-data-dir':
      command     => "setfacl -R -M ${acls} ${data_dir}",
      require     => [ File[$acls, $data_dir], Package['acl'] ],
      subscribe   => File[$acls],
      refreshonly => true,
  }
  exec {
    'set-acl-on-rsync-tmp-dir':
      command     => "setfacl -R -M ${acls} ${tmpdir}",
      require     => [ File[$acls, $tmpdir], Package['acl'] ],
      subscribe   => File[$acls, $tmpdir],
      refreshonly => true,
  }

  $release_managers.each |String $release_manager| {
    user::groups::manage_user { "${release_manager}_in_rsync_tails":
      user    => $release_manager,
      group   => 'rsync_tails',
      require => User['rsync_tails'],
    }
  }

}
