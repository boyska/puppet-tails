# Manages the Tails Jenkins master.
# Installs extra packages for nice features as well as Jenkins plugins.
# If $automatic_iso_jobs_generator is 'present', $jenkins_jobs_repo must be
# set to the URL of a jenkins-jobs git repo where the 'jenkins@jenkins-master'
# SshKey has write access. It depends on $deploy_on_git_push being set to true
# for the pushed configuration to be applied automatically.
# For this to happen, hooks managed in tails::gitolite::hooks::jenkins_jobs
# also need to be installed in the jenkins-jobs repo.

class tails::jenkins::master (
  String $jenkins_jobs_repo,
  String $api_token,
  String $version                                         = '2.289.2',

  String $tails_repo                                      = 'https://gitlab.tails.boum.org/tails/tails.git',
  Boolean $deploy_jobs_on_git_push                        = true,
  Enum['present', 'absent'] $automatic_iso_jobs_generator = 'present',
  Integer $active_branches_max_age_in_days                = 49,
  String $gitolite_pubkey_name                            = 'gitolite@puppet-git',
  String $api_user                                        = 'Tails',

  Boolean $manage_mount                                   = false,
  $mount_device                                           = false,
  $mount_fstype                                           = 'ext4',
  $mount_options                                          = 'relatime,user_xattr,acl',

) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '9') < 0 {
    fail('This module only supports Debian 9 or newer.')
  }

  ### Variables

  $mount_point = '/var/lib/jenkins'
  $ssh_pubkey_name = "jenkins@jenkins-master.${::domain}"

  ### Resources

  apt::pin { 'jenkins':
    packages => 'jenkins',
    version  => $version,
    priority => 991,
  }

  class { 'jenkins':
    repo            => true,
    lts             => true,
    install_java    => false,
    version         => $version,
    default_plugins => [],
    require         => [
      Package[$base_packages],
      Apt::Conf['proxy_jenkins_repo'],
      Apt::Pin['jenkins'],
    ],
  }

  # Provides the jar command, which is used by Jenkins::Cli/Exec[jenkins-cli]
  package { 'default-jdk-headless':
    ensure => installed,
  }

  # apt-cacher-ng does not support HTTPS repositories
  apt::conf { 'proxy_jenkins_repo':
    content  => 'Acquire::HTTP::Proxy::pkg.jenkins.io "DIRECT";',
  }
  apt::conf { 'proxy_prodjenkinsreleases_repo':
    content  => 'Acquire::HTTP::Proxy::prodjenkinsreleases.blob.core.windows.net "DIRECT";',
  }

  apt::pin { 'jenkins-job-builder':
    packages   => [
      'jenkins-job-builder',
      'python3-jenkins-job-builder',
      'python3-jenkins'
    ],
    originator => 'Debian Backports',
    priority   => 991,
  }

  $base_packages = [
    'git',
    'jenkins-job-builder',
    'libmockito-java',
    'python3-jenkins-job-builder',
  ]

  ensure_packages($base_packages)

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)
#    validate_string($monitoring_parent_zone)

    # Needs to be created by hand before applying this, if $manage_mount
    # is true. We cannot manage File[$mount_point] ourselves as this
    # would duplicate the same declaration in jenkins::config.
    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

    Mount[$mount_point] -> Class['jenkins']

  }

  include nfs::server

  Nfs::Export <<| tag == $::fqdn |>>

  # lint:ignore:140chars -- SHA512

  jenkins::plugin { 'apache-httpcomponents-client-4-api':
    version       => '4.5.13-1.0',
    digest_string => '1fa5adafcd043c582a9b6ec44a6a37166db77bb6d5b7d2c4f9902ad263a65636',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'bootstrap4-api':
    version       => '4.6.0-3',
    digest_string => '5ed055fc291662a02a01d3763aef9de5f3f0fb86ebe946e939a5dc27a87bd513',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'font-awesome-api',
      'jquery3-api',
      'popper-api',
    ],
  }

  jenkins::plugin { 'bootstrap5-api':
    version       => '5.0.2-1',
    digest_string => '9e264940da4497494e3c24d96f83fdde95ec405b71bec8b5976c2fc5857e7e55',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'font-awesome-api',
      'popper2-api',
    ],
  }

  jenkins::plugin { 'build-symlink':
    version       => '1.1',
    digest_string => 'dc5e517743d872ceefb86199d18c68bef4885c02c275249308a7b37ded8d4504',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'build-timeout':
    version       => '1.20',
    digest_string => '96f6ef2a0486a9a13d44168aed24037c65f8f8fe670a1223956bca1c6a2e073b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'caffeine-api':
    version       => '2.9.1-23.v51c4e2c879c8',
    digest_string => '5bcc5240443e826ea39b258fd67e6c497fbbd4ec47a316fbcbddfdbdf4bc876b',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'checks-api':
    version       => '1.7.2',
    digest_string => 'effbec009c054395fd6052edea58887a16f3c4f96fbb86a14933fa9bcc9a8d0b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'plugin-util-api',
      'workflow-step-api',
      'workflow-support',
      'display-url-api',
    ],
  }

  jenkins::plugin { 'cloudbees-folder':
    version       => '6.15',
    digest_string => 'bea514f622bd9375a55646f0189f6ca81195790fb59299b877291a81248ed3b4',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'f09b82ccec2afc60c3b9235d804f312c3e5a0a847b4243b1e3546f718f344af1f7a0f26c4d53a02ae360aa6a50a20676910b143011c4eb880daa8ab0bc0fb073',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'conditional-buildstep':
    version       => '1.4.1',
    digest_string => 'ebdd679e976af3062aac44eeef6b32529e4d91ab816c28db0c763bf111525a4c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'maven-plugin',
      'run-condition',
      'token-macro',
    ],
  }

  jenkins::plugin { 'copyartifact':
    version       => '1.46.1',
    digest_string => '3b2095789e8d6523f6d825bafacf9f8b09f4fc737b5d9bf997dfca6b42c8f44e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials':
    version       => '2.5',
    digest_string => 'ee6e1055e33cbd00ab59491b0303589f7f0c4060298ddcd097c502abbe32d228',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'credentials-binding':
    version       => '1.27',
    digest_string => 'b909a9a49fb16d7ff9d5c7a5213263b75afeed50b3434266b195a81e47fcd7e3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'workflow-step-api',
      'credentials',
      'plain-credentials',
      'ssh-credentials',
      'structs',
    ],
  }

  jenkins::plugin { 'cucumber-reports':
    version       => '5.5.0',
    digest_string => 'ee20ef746299cc5f6bbc95823dbc85f6dced7133fca80f492eee0f7cc6b226d2',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'cucumber-testresult-plugin':
    version       => '0.10.1',
    digest_string => '2d2f171a8561ec91a11def39a3f1e75302516f097e86157f5bdd1402f29858bf',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'display-url-api':
    version       => '2.3.5',
    digest_string => '4a418820b9faa7f01cfa990b75ddc67f090809d3f9bd8b95707cd7840eb45209',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'downstream-ext':
    version       => '1.8',
    digest_string => '5033490d9b34943488e387d64a3a09cf02a43dced29b0f43e8a68b9d837a1869702f9080831fc80e64e2addebf0553bd5c6a8793b46182ed1535422ca839d27f',
    digest_type   => 'sha512',
  }

  jenkins::plugin { 'durable-task':
    version       => '1.38',
    digest_string => '69587f60e456a8a0b5f47fe7055822acebfaaf5c95dfb436bfdab3f5907c4949',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'echarts-api':
    version       => '5.1.2-3',
    digest_string => '44b9229fd045182b850e790b7e600815cc3d7be9f90ce56a66d39f5a9026d98f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap5-api',
      'font-awesome-api',
      'jquery3-api',
      'plugin-util-api',
      'jackson2-api',
    ],
  }

  jenkins::plugin { 'email-ext':
    version       => '2.83',
    digest_string => 'b1f5103b04cc4e4b1347eb79ebcf9ad9fc963b474f1cacbe0f90da650241ea8a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'jackson2-api',
      'junit',
      'mailer',
      'matrix-project',
      'script-security',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '2.4.0',
    digest_string => '0ba104848ffa8c3b55a1a9d4876748152dde234f49b3da7b06f7d4902a38824d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'envinject-api',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'envinject-api':
    version       => '1.7',
    digest_string => '0fb5c4d0b0fbc112addedd604a94d94ce5c42dde3cc6b2494bbf706cdb249a9b',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'font-awesome-api':
    version       => '5.15.3-4',
    digest_string => '2ded09a76b3f26ebeb607b00d981b9abcbdd7722c51eb3c7f910635698f6d234',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'plugin-util-api',
    ],
  }

  jenkins::plugin { 'git':
    version       => '4.8.0',
    digest_string => '785c49dfeb9c9caead7c7499f59e226d99e7a514e69582c8fdb3fbd495348907',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'credentials-binding',
      'git-client',
      'mailer',
      'matrix-project',  # an optional dependency
      'scm-api',
      'script-security',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '3.8.0',
    digest_string => 'b2e26b964e1a0d841a3f7e202b45926f199d98c6fc3316139f99bd571b64e832',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'credentials',
      'jsch',
      'script-security',
      'ssh-credentials',
      'structs',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '1.5',
    digest_string => '36b1aeecd6f6cd96263baca7143e4e201f93cc4797b814e62527cf58a9fd4b82',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',
    ],
  }

  jenkins::plugin { 'javadoc':
    version       => '1.6',
    digest_string => 'a5f2321aed9ec068781962114413f305665dfbcc89ba13bee9b797551de6bbc9',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jackson2-api':
    version       => '2.12.4',
    digest_string => 'f5961521396f02c0518af70816bf6b2c4d12a4ef7194a1d2db65235eff609602',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['snakeyaml-api'],
  }

  jenkins::plugin { 'jquery3-api':
    version       => '3.6.0-2',
    digest_string => '09b75305611160148c572f76953a020278d64960768cb76f5e435a665170de4e',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'jsch':
    version       => '0.1.55.2',
    digest_string => 'cdc74bf8e43eb40ae6ad98ba2f866c8891408038699da9b836518a1d8923fc44',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'ssh-credentials',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'junit':
    version       => '1.51',
    digest_string => '04b00d3145d0e260b58a4e9d1fc2fa60626dbf5e830121650cd59fa2609fdec9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap4-api',
      'checks-api',
      'display-url-api',
      'echarts-api',
      'jackson2-api',
      'plugin-util-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'mailer':
    version       => '1.34',
    digest_string => 'a38b98400d19155688471f3ff57390353f56e2773f8476e7bb78ed7a6f291d29',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'display-url-api',
    ],
  }

  jenkins::plugin { 'matrix-auth':
    version       => '2.6.8',
    digest_string => 'af6e0ed707e6310ef5ca29636883ecde9aef978ea1173315e46528bdca2ec284',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',  # optional dependency
    ],
  }

  jenkins::plugin { 'matrix-project':
    version       => '1.19',
    digest_string => 'c2a654a84f8c57bb810101cefca29223a35045f6aefae852501f8597449ea49f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'script-security',
    ],
  }

  jenkins::plugin { 'maven-plugin':
    version       => '3.12',
    digest_string => '5463c9de1730855d66a501d6f8c6bf119f061cb1ebea4cbf70d67692894fbe1a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'javadoc',
      'jsch',
      'junit',
      'mailer',
    ],
  }

  jenkins::plugin { 'parameterized-trigger':
    version       => '2.41',
    digest_string => '29a6f937184c82b7db8da072396fd34e807cbfa311515d37c35bfe06da64b37c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'conditional-buildstep',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'plain-credentials':
    version       => '1.7',
    digest_string => '5a82ca66b397daccf663695b531fdf7ea993d5c1cb03f1b4692dbe804d163ba8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['credentials'],
  }

  jenkins::plugin { 'plugin-util-api':
    version       => '2.4.0',
    digest_string => '8fe46dbbbcc2d7b8db3e1511196df5bba068204e4406b45942995a14a59d5b53',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'popper-api':
    version       => '1.16.1-2',
    digest_string => 'c5bbf6279fa3492724e86376da2ecc038a27b089287012f6f11b50b6ab65e3f2',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'popper2-api':
    version       => '2.5.4-3',
    digest_string => '9f0124cf0f9b54382b095964751560013ed455faea746efbce1e1c1d0f954e87',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'postbuildscript':
    version       => '3.0.0',
    digest_string => '228b66c391ee7ef2fc7c8589900c92cb584e324dd27422212f96bdff0e3f3303',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
    ],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '4.0.0',
    digest_string => 'f691dac459f7fd40c65d1feb427fc73f6a4f284259f38cb3ff0ac1f5aeccda17',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'resource-disposer':
    version       => '0.16',
    digest_string => 'fd924c8001799de12b0cae5dd86be230b27c581356ec9acf4c6ab4d0c842faaa',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'run-condition':
    version       => '1.5',
    digest_string => '7ed94d7196676c00e45b5bf7e191831eee0e49770dced1c266b8055980b339ca',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['token-macro'],
  }

  jenkins::plugin { 'scm-api':
    version       => '2.6.4',
    digest_string => '935cbae4b46678d5adf9faac9a68029f52a04e0d0c89c9038a7ab95ca6b676dc',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'scoring-load-balancer':
    version       => '1.0.1',
    digest_string => 'a7229d2945e347afb472d3c45e83ea3c4409c8710c4168912601eb46684dd3a3',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'script-security':
    version       => '1.77',
    digest_string => '7ec9a146920291a499d510a7025125922d37334a1e844b195e12a0c9e8764463',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['caffeine-api'],
  }

  jenkins::plugin { 'simple-theme-plugin':
    version       => '0.7',
    digest_string => 'bf75f3d1aea7d02dedc76c42b9bcddb4ce35a98bec77b4c9cadd6ebdd6b396c6',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'snakeyaml-api':
    version       => '1.29.1',
    digest_string => '9f8b34f1e657a4e88349663988b405e2dead6b1f9e737e8fac9736a69b6e5595',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '1.19',
    digest_string => '6dc93c4ac8669d3851ea23b15d675b973f0c042658d2e8cbfa868eed8549ed15',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'structs':
    version       => '1.23',
    digest_string => '0d53d44b4ce7f887436921a81321f1738de7391a35912c1096fe412df4d6338a',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'timestamper':
    version       => '1.13',
    digest_string => '7178583d5660b282e957d252896b205abdcd03ace74d79dd423abd6a99c4cef0',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'token-macro':
    version       => '2.15',
    digest_string => 'eb1b796267789c6038085237ab9fbf7a6637ca7a56ab33f1c6261fd58e581d91',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'trilead-api':
    version       => '1.0.13',
    digest_string => '62b46838e493cfc6950382b4e4975d865cd3bfbfc58ada704b9dd96ff101e457',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'workflow-api':
    version       => '2.46',
    digest_string => '50ef8934366ca61295113f5bd85303222cb8001049aa219934ac61b1f51bb507',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-durable-task-step':
    version       => '2.39',
    digest_string => 'c2cfa846ae0e4e0f50db77f18d7d84bf32b77576a5b2d05a151040a8565d8b55',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'durable-task',
      'scm-api',
      'script-security',
      'structs',
      'workflow-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'workflow-step-api':
    version       => '2.24',
    digest_string => 'e6d629ebdc248d2483069b5c67d1337df5ebc953600fbaed3b4d9e65dde34266',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['structs'],
  }

  jenkins::plugin { 'workflow-support':
    version       => '3.8',
    digest_string => 'e88769cb88a05277a2df8d8793bba34550f223e27161bfb44194c0918ec93138',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'scm-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-scm-step':
    version       => '2.13',
    digest_string => '6a97728795e3a77104760e5fca74d42c3d20755173675f706196ac142209c5e4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin['workflow-step-api'],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.39',
    digest_string => '7bc8f0058aee2f25e15eaf31a2de1816d251de9953933a35c702b05f0016e4f3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
      'structs',
      'resource-disposer',
    ],
  }
  # lint:endignore

  ## Uncomment this (or similar) once all this is moved to a proper class,
  ## that inherits jenkins::service and can thus append to its dependencies.
  # Service['jenkins'] {
  #   require +> File_line['jenkins_HTTP_HOST'],
  # }

  file { '/etc/jenkins':
    ensure  => directory,
    mode    => '0750',
    owner   => 'root',
    group   => 'jenkins',
    require => User['jenkins'],
  }

  # Used by the reboot_node macro
  file { '/etc/jenkins/jenkins_apikey':
    ensure  => present,
    content => $api_token,
    mode    => '0640',
    owner   => 'root',
    group   => 'jenkins',
  }

  file { '/etc/jenkins_jobs/jenkins_jobs.ini':
    owner   => root,
    group   => jenkins,
    mode    => '0640',
    content => template('tails/jenkins/orchestrator/jenkins_jobs.ini.erb'),
    require => [
      Package['jenkins'],
      Package['jenkins-job-builder'],
    ],
  }

  file { '/etc/jenkins_jobs':
    ensure => directory,
    owner  => root,
    group  => jenkins,
    mode   => '0770',
  }

  vcsrepo { '/etc/jenkins_jobs/jobs':
    ensure   => present,
    owner    => jenkins,
    group    => jenkins,
    user     => jenkins,
    provider => git,
    source   => $jenkins_jobs_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
      Package['git'],
      File['/etc/jenkins_jobs'],
    ],
  }

  if $deploy_jobs_on_git_push {
    file { '/var/tmp/jenkins_jobs_test':
      ensure => directory,
      owner  => jenkins,
      group  => jenkins,
      mode   => '0700',
    }

    file { '/usr/local/sbin/deploy_jenkins_jobs':
      ensure  => present,
      source  => 'puppet:///modules/tails/jenkins/master/deploy_jenkins_jobs',
      owner   => root,
      group   => root,
      mode    => '0755',
      require => [
        File['/var/tmp/jenkins_jobs_test'],
        Ssh_authorized_key[$gitolite_pubkey_name],
      ],
    }

    sshkeys::set_authorized_keys { $gitolite_pubkey_name:
      user    => jenkins,
      home    => '/var/lib/jenkins',
      require => Package['jenkins'],
    }
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts':
    owner  => root,
    group  => root,
    mode   => '0755',
    source => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts',
  }

  file { '/usr/local/bin/clean_old_jenkins_artifacts_wrapper':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/clean_old_jenkins_artifacts_wrapper',
    require => File['/usr/local/bin/clean_old_jenkins_artifacts'],
  }

  cron { 'clean_old_jenkins_artifacts':
    command => '/usr/local/bin/clean_old_jenkins_artifacts_wrapper /var/lib/jenkins',
    user    => 'jenkins',
    hour    => '23',
    minute  => '50',
    require => [File['/usr/local/bin/clean_old_jenkins_artifacts_wrapper'],
                Package['jenkins']],
  }

  file { '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/deduplicate_reproducible_build_jobs_upstream_ISOs',
    require => Package['jenkins'],
  }

  cron { 'deduplicate_reproducible_build_jobs_upstream_ISOs':
    command => '/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/6',
    require => File['/usr/local/bin/deduplicate_reproducible_build_jobs_upstream_ISOs'],
  }

  file { '/usr/local/bin/manage_latest_iso_symlinks':
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/jenkins/master/manage_latest_iso_symlinks',
    require => Package['jenkins'],
  }

  cron { 'manage_latest_iso_symlinks':
    command => '/usr/local/bin/manage_latest_iso_symlinks /var/lib/jenkins/jobs',
    user    => 'jenkins',
    minute  => '*/5',
    require => File['/usr/local/bin/manage_latest_iso_symlinks'],
  }

  class  { 'tails::jenkins::iso_jobs_generator':
    ensure            => $automatic_iso_jobs_generator,
    tails_repo        => $tails_repo,
    jenkins_jobs_repo => $jenkins_jobs_repo,
    active_days       => $active_branches_max_age_in_days,
    require           => [
      Class['jenkins'],
      Sshkeys::Set_client_key_pair[$ssh_pubkey_name],
    ],
  }

  file { '/var/lib/jenkins/.ssh':
    ensure  => directory,
    owner   => jenkins,
    group   => jenkins,
    mode    => '0700',
    require => Class['jenkins'],
  }

  sshkeys::set_client_key_pair { $ssh_pubkey_name:
    keyname => $ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
    require => File['/var/lib/jenkins/.ssh'],
  }

  postfix::mailalias { 'jenkins':
    recipient => 'root',
  }
}
