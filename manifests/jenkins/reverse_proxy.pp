# Manage a nginx vhost that reverse proxies to a Jenkins instance.
class tails::jenkins::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'jenkins.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'jenkins.lizard',
  Stdlib::Port $upstream_port     = 8080,
  Stdlib::Port $upstream_api_port = 42585,
  ) {

  ### Sanity checks

  if $::osfamily != 'Debian' {
    fail('The tails::jenkins::reverse_proxy class only supports Debian.')
  }

  ### Resources

  $upstream_address = "${upstream_hostname}:${upstream_port}"
  $upstream_api_address = "${upstream_hostname}:${upstream_api_port}"

  nginx::vhostsd { $public_hostname:
    content => template('tails/jenkins/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      Nginx::Authd['jenkins'],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included['tails_jenkins_reverse_proxy'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { 'tails_jenkins_reverse_proxy':
    content => template('tails/jenkins/nginx/reverse_proxy.inc.erb'),
  }

  nginx::authd { 'jenkins':
    source  => "puppet:///modules/site_jenkins/nginx/${::fqdn}/auth.d/htpasswd.jenkins",
  }

  tails::letsencrypt::certonly { $public_hostname: }

  # Jenkins agents hosted in lizard communicate with the controller using the
  # public JENKINS_URL (https://jenkins.tails.boum.org), as configured in the
  # Jenkins web interface. That hostname is locally resolved in agents to
  # lizard's LAN ip (192.168.122.1). Those connections are then redirected to
  # www.lizard by the host firewall, and need to be served without
  # authentication. We use alternative ports for that (1180, 11443) which are
  # not made available publicly.

  nginx::included { 'tails_jenkins_reverse_proxy_noauth':
    content => template('tails/jenkins/nginx/reverse_proxy.inc.noauth.erb'),
  }

  nginx::included { 'tails_jenkins_reverse_proxy_api_noauth':
    content => template('tails/jenkins/nginx/reverse_proxy.inc.api_noauth.erb'),
  }

  nginx::vhostsd { "${public_hostname}-noauth":
    content => template('tails/jenkins/nginx/reverse_proxy.vhost.noauth.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included['tails_jenkins_reverse_proxy_noauth'],
      Nginx::Included['tails_jenkins_reverse_proxy_api_noauth'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

}
