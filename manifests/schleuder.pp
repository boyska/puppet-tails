# Manage Schleuder and its MTA
class tails::schleuder (
  String $superadmin_alias,
  Array[String] $superadmins,
  String        $myorigin         = 'mail.tails.boum.org',
  String        $schleuder_domain = 'boum.org',
  String        $root_recipient   = lookup(tails::profile::base::root_mail_recipient,undef,undef,{}),
  Hash          $lists            = {},
) {

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::schleuder class only supports Debian Stretch.')
  }

  ### Basic Postfix setup

  class { '::tails::postfix':
    mydestination       => "\$myhostname, \$myorigin, ${::fqdn}, localhost",
    myorigin            => $myorigin,
    root_mail_recipient => $root_recipient,
    smtp_listen         => 'all',
    use_spamassassin    => true,
    use_schleuder       => true,
    local_domains       => [$schleuder_domain, ".${schleuder_domain}"],
  }

  ### TLS

  file { "/etc/ssl/${myorigin}":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0500',
  }

  # Manage {cert,fullchain,privkey}.pem by collecting resources
  # exported by tails::mail::webserver
  File <<| tag == 'tails_mail_tls' |>>

  postfix::config { 'smtpd_tls_cert_file':
    value => "/etc/ssl/${myorigin}/cert.pem",
  }
  postfix::config { 'smtpd_tls_CAfile':
    value => "/etc/ssl/${myorigin}/fullchain.pem",
  }
  postfix::config { 'smtpd_tls_key_file':
    value => "/etc/ssl/${myorigin}/privkey.pem",
  }

  ### Virtual addresses

  postfix::config { 'virtual_mailbox_domains':
    value => $schleuder_domain,
  }

  ### Transport

  # Note: postfix::mta already manages transport_maps and /etc/postfix/transport
  postfix::transport { 'boum.org':
    destination => 'smtp',
    nexthop     => 'boum.org',
  }

  ### Schleuder

  postfix::config { 'schleuder_destination_recipient_limit':
    value => '1',
  }

  postfix::mailalias { $superadmin_alias: recipient => $superadmins }

  $packages_from_stretch_backports = [
    'dirmngr',
    'gnupg',
    'gnupg-agent',
    'gnupg-l10n',
    'gnupg-utils',
    'gpg',
    'gpgconf',
    'gpgsm',
    'gpgv',
    'gpg-agent',
    'gpg-wks-client',
    'gpg-wks-server',
    'libassuan0',
    'libgpg-error-l10n',
    'libgpg-error0',
    'schleuder',
    'schleuder-cli',
    'ruby-gpgme',
    'ruby-mail-gpg',
  ]
  $packages_from_stretch_backports.each |String $package| {
    apt::pin { $package:
      packages   => $package,
      originator => 'Debian Backports',
      codename   => 'stretch-backports',
      priority   => 991,
    }
  }
  class{'schleuder':
    cli_api_key   => sha1("${fqdn_rand(1204, 'cli')}"),
    gpg_use_tor   => true,
    lists         => $lists,
    superadmin    => $superadmin_alias,
    gpg_keyserver => 'hkps://keys.openpgp.org',
    require       => Apt::Pin[$packages_from_stretch_backports],
  }

  keys($lists).each |String $list_name| {
    ['', '-request', '-owner', '-bounce', '-sendkey'].each |String $suffix| {
      postfix::transport { join(split($list_name, '@'), "${suffix}@"):
        destination => 'schleuder',
      }
    }
  }

  ### Whitelist for automated messages

  $filters_dir = '/usr/local/lib/schleuder/filters'

  file { "${filters_dir}/pre_decryption":
    ensure => directory,
    owner  => 'root',
    group  => 'staff',
    mode   => '0755',
  }

  file { "${filters_dir}/pre_decryption/01_automated_messages_whitelist.rb":
    ensure  => present,
    source  => 'puppet:///modules/tails/schleuder/01_automated_messages_whitelist.rb',
    owner   => 'root',
    group   => 'staff',
    mode    => '0644',
    require => File["${filters_dir}/pre_decryption"],
    notify  => Service['schleuder-api-daemon'],
  }

}
