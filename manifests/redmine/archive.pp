# Manage static archives of Tails' Redmine
class tails::redmine::archive (
  String $http_password_hash,
  Enum['present', 'absent'] $ensure      = 'present',
  String $http_user                      = 'Tails',
  Stdlib::Fqdn $public_archive_hostname  = 'public-redmine-archive.tails.boum.org',
  Stdlib::Fqdn $private_archive_hostname = 'private-redmine-archive.tails.boum.org',
) {

  tails::redmine::archive_vhost { $public_archive_hostname:
    public             => true,
  }

  tails::redmine::archive_vhost { $private_archive_hostname:
    public             => false,
    http_user          => $http_user,
    http_password_hash => $http_password_hash,
  }

}
