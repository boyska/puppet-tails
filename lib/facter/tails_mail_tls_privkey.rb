Facter.add('tails_mail_tls_privkey') do
  setcode do
    file = '/etc/letsencrypt/live/mail.tails.boum.org/privkey.pem'
    File.exists?(file) ? File.read(file) : nil
  end
end
