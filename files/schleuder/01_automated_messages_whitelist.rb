module Schleuder
  module Filters

    # Schleuder filters automated messages, but we want to accept the ones
    # coming from GitLab. For this, the ugly workaround is to modify the
    # original message and set the 'Auto-Submitted' header to 'no' when certain
    # criteria is found.
    #
    # See: https://gitlab.tails.boum.org/tails/sysadmin/-/issues/17714

    def self.automated_messages_whitelist(list, mail)
      if mail[:auto_submitted] && mail[:auto_submitted].to_s.downcase != 'no' &&
          (mail[:x_gitlab_project] || mail[:x_weblate_notification])
        mail[:auto_submitted].value = 'no'
      end
    end

  end
end
