#!/bin/bash
#
# Import test projects and run all tests for our custom scripts.

set -ex

import_test_projects() {
	WEBLATE="sudo -u weblate podman exec weblate weblate"

	# The database is already setup on application deployment, during
	# container startup, so we can directly configure Weblate here.
	${WEBLATE} shell -c "from weblate.trans.models.project import Project; Project(name='Tails', slug='tails', web='https://tails.boum.org').save()"

	# The Weblate project `Tails2` is used in tests
	${WEBLATE} shell -c "from weblate.trans.models.project import Project; Project(name='Tails2', slug='tails2', web='https://tails.boum.org').save()"

	# Puppet cloned the Tails repository into the Weblate VCS directory for
	# the `index` component, but `import_project` fails to run if the
	# directory alredy exists.
	rm -rf /var/lib/weblate/data/vcs/tails/index

	# Some notes about project importing:
	#
	#   - We don't import all languages here so importing is quicker.
	#
	#   - The main component in production website is currently `index`.
	#
	#   - Our components are scattered under wiki/src/, so we need to
	#     explicitly allow subdirectories in the regexp.
	${WEBLATE} import_project \
		--language-regex '^(de|fr)$' \
		--main-component index \
		tails https://gitlab.tails.boum.org/tails/tails.git master \
		"wiki/src/(?P<component>([^/]+\/)*.+)\.(?P<language>[^\.]+)\.po"
}

run_tests() {
	PODMAN="sudo -u weblate podman"

	# Tests need the `weblate.settings` Python module to be available
	${PODMAN} exec --user=root weblate ln -sf /usr/local/lib/python3.7/dist-packages/weblate/{settings_docker,settings}.py
	${PODMAN} exec --user=root weblate apt-get -y install python3-nose python3-yaml
	${PODMAN} exec weblate python3 -m nose --verbose /scripts/
}

main() {
	import_test_projects
	run_tests
}

main
