#!/bin/bash
#
# Deploy Weblate using our Puppet code.

set -ex

DIRNAME=$( dirname ${0} )
WORKDIR=/tmp
MODULEPATH=${WORKDIR}/modules
PUPPET_APPLY="puppet apply --modulepath=${MODULEPATH}"

install_dependencies() {
	apt-get update
	apt-get -y install puppet git r10k curl python3-yaml
	mkdir -p ${MODULEPATH}
	r10k puppetfile install --puppetfile=${DIRNAME}/Puppetfile

	# We also want to have the code from this repository as a module.
	ln -sf $( git -C ${DIRNAME} rev-parse --show-toplevel ) ${MODULEPATH}/tails

	# On startup, the Weblate container checks if port 25 is open and
	# only return HTTP 400's if it's not.
	DEBIAN_FRONTEND=noninteractive apt-get -q -y install postfix
}

hiera_contains() {
	key="$1"
	cat ${MODULEPATH}/tails/data/common.yaml \
		| python3 -c "import yaml, sys; sys.exit(int('${key}' not in yaml.safe_load(sys.stdin)))"
}

deploy_application() {

	# Puppet fails when it can't find mandatory class parameters, so we set
	# them here.
	for param in postgres_password weblate_admin_password weblate_secret_key redis_password; do
		if ! hiera_contains tails::weblate::${param}; then
			echo "tails::weblate::${param}: insecure" >> ${MODULEPATH}/tails/data/common.yaml
		fi
	done


	# In order for the scripts to be available inside the container, their
	# directory needs to be passed on container creation.
	SCRIPTS_DIR="$( git -C ${DIRNAME} rev-parse --show-toplevel )/files/weblate/scripts"

	# Let's create some aliases to shorten the commands below. We need to
	# include tails::apt before weblate because tails::website::builder
	# depends on it.
	TAILS_APT_CLASS="class {'tails::apt': cron_mode => 'update', codename => 'bullseye', proxy => false, debian_url => 'http://deb.debian.org/debian' }"
	TAILS_WEBLATE_CLASS="class { 'tails::weblate': podman_extra_volumes => ['${SCRIPTS_DIR}:/scripts:Z'] }"

	# Make sure APT is configured and updated before deploying the service
	${PUPPET_APPLY} -e "${TAILS_APT_CLASS}"
	apt-get update

	# The first Puppet run fails because two of the integration repository
	# remotes are inaccessible: one of them can only be accessed in the
	# production environment and the other can only be accessed from inside
	# the container (while Puppet runs in the host). Because of how vcsrepo
	# works, it will correctly configure the remotes in the repo, but will
	# fail when trying to fetch from them. This will only raise an error in
	# Puppet the first time it's applied, and should not be a problem in
	# subsequent runs.
	${PUPPET_APPLY} -e "${TAILS_APT_CLASS}; ${TAILS_WEBLATE_CLASS}"

	# The second Puppet run fails because of how the Puppet Podman module
	# is written: a container service reload is attempted before systemd is
	# actually aware of the newly created `podman-weblate.service` unit
	# file. This shuold not happen again in subsequent runs.
	${PUPPET_APPLY} -e "${TAILS_APT_CLASS}; ${TAILS_WEBLATE_CLASS}"

	# The third Puppet run finally succeeds!
	${PUPPET_APPLY} -e "${TAILS_APT_CLASS}; ${TAILS_WEBLATE_CLASS}"
}

wait_for_application() {

	DOMAIN="translate.tails.boum.org"  # Otherwise app returns HTTP 400
	WAITED=0
	TIMEOUT=300
	SLEEP=10

	# Weblate takes a while to come up, so we want to proceed only after it
	# returns success on HTTP. We use port 80 because that's where Nginx is
	# listening, so the reverse proxy must also be working.
	while ! curl -f -s -I --resolve ${DOMAIN}:80:127.0.0.1 http://${DOMAIN}:80; do
		[ ${WAITED} -eq ${TIMEOUT} ] && break
		#sudo -u weblate podman logs weblate | tail
		echo "Waiting for application... (${WAITED}/${TIMEOUT})"
		WAITED=$(( ${WAITED} + ${SLEEP} ))
		sleep ${SLEEP};
	done

	[ ${WAITED} -lt ${TIMEOUT} ] || exit 1
	echo "Application is up!"
}

deploy_weblate() {
	install_dependencies
	deploy_application
	wait_for_application
}

main() {
	deploy_weblate
}

main
