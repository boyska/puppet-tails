#!/usr/bin/python3
import unittest
import subprocess

class MeetingTest(unittest.TestCase):
    faketime = "faketime"
    command = ['./meeting.py', '--print-schedule',
               '--from', 'noreply@tails.boum.org',
               '--addresses', 'noreply@tails.boum.org',
               '--subject', 'Tails Foundations Team meeting: ',
               '--template', 'FT_meeting_template.eml']
    date = "2018-12-23"

    def test1(self):
        date = "2008-12-24"
        output = subprocess.check_output([self.faketime, date, *self.command, '--skip-friday-to-sunday'])
        self.assertEqual(output, b'The next occurrence of this event will happen on Tuesday 06. January 2009\n')

    def test2(self):
        output = subprocess.check_output([self.faketime, self.date, *self.command])
        self.assertEqual(output, b'The next occurrence of this event will happen on Thursday 03. January 2019\n')

    def test3(self):
        output = subprocess.check_output([self.faketime, self.date, *self.command, '-m', '3', '--skip-friday-to-sunday'])
        self.assertEqual(output, b'The next occurrence of this event will happen on Wednesday 06. March 2019\n')

    def test4(self):
        output = subprocess.run([self.faketime, self.date, *self.command, '-d', '32'], check=False, capture_output=True)
        self.assertEqual(output.stderr, b'The month/day combination month: 12, day: 32 is invalid: day is out of range for month\n')

    def test5(self):
        output = subprocess.check_output([self.faketime, self.date, *self.command, '-m', '1', '-d', '27', '--skip-friday-to-sunday'])
        self.assertEqual(output, b'The next occurrence of this event will happen on Wednesday 30. January 2019\n')

    def test6(self):
        date = "2008-11-24"
        output = subprocess.check_output([self.faketime, date, *self.command])
        self.assertEqual(output, b'The next occurrence of this event will happen on Wednesday 03. December 2008\n')

    def test7(self):
        date = "2008-01-24"
        output = subprocess.check_output([self.faketime, date, *self.command, '--skip-friday-to-sunday'])
        self.assertEqual(output, b'The next occurrence of this event will happen on Wednesday 06. February 2008\n')

if __name__ == '__main__':
    unittest.main()
