# Tests for feedfilters.py
#
# Currently, the only implemented filter aims to drop messages that do not
# contain the case-insensitive word "security" either in their subject or in
# their body (assuming rss2email has been configured to send plain text
# messages).
#
# Here we do simple tests where the filter should be able to let messages pass
# if they match the above description or return None otherwise.

import sys
from email.message import EmailMessage
from email.headerregistry import Address
from feedfilters import drop_non_security


def _message(subject="A subject", content="Arbitrary text.\n"):
    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = Address("Derya", "derya", "example.com")
    msg['To'] = Address("Riou", "riou", "example.com")
    msg.set_content(content)
    return msg


def test_accept_message_with_lowercase_security_in_payload():
    m = _message(content="Description security fixes")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with lowercase "security" in its payload'


def test_accept_message_with_mixed_case_security_in_payload():
    m = _message(content="Description of SeCuRiTy fixes")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with mixed-case "security" in its payload'

def test_accept_message_with_uppercase_security_in_payload():
    m = _message(content="Description of SECURITY fixes")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with uppercase "security" in its payload'


def test_accept_message_with_lowercase_security_in_subject():
    m = _message(subject="Updates bring more security for Tails!")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with lowercase "security" in its subject'


def test_accept_message_with_mixed_case_security_in_subject():
    m = _message(subject="sEcUrItY updates for Tails!")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with mixed-case "security" in its subject'


def test_accept_message_with_uppercase_security_in_subject():
    m = _message(subject="SECURITY updates for Tails!")
    assert drop_non_security(m) == m, \
           'Failed recognizing message with uppercase "SECURITY" in its subject'


def test_filter_message_with_no_mention_of_security():
    m = _message()
    assert drop_non_security(m) == None, \
           'Failed to filter out message with no mention of "security" at all'
