"""
rss2email custom post-process filters.
"""

import re


def drop_non_security(message, **kwargs):
    """
    Return the message only if it contains a case-insensitive 'security' word
    in either its subject or payload. Else, return None.
    """
    payload = message.get_payload()
    subject = message['subject']
    m = re.compile('security', re.IGNORECASE)
    if any(map(m.search, [payload, subject])):
        return message
    return None
