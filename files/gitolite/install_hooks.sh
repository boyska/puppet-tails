#!/bin/bash
set -e

gitolite_root=/var/lib/gitolite3
repository_root="${gitolite_root}/repositories"
tails_common_hookdir="${gitolite_root}/.gitolite/hooks/tails_common"

install_post_update_dot_d_hook () {
    repository="$1"
    if [ ! -d "${repository}/hooks/post-update.d" ]; then
        mkdir "${repository}/hooks/post-update.d"
        chown gitolite3:gitolite3 "${repository}/hooks/post-update.d"
    fi
    if [ ! -h "${repository}/hooks/post-update" ]; then
        /bin/ln -s "${tails_common_hookdir}/post_update.d.hook" "${repository}/hooks/post-update"
    fi
}

install_post_update_hook () {
    repository="$1"
    type="$2"
    remote="$3"
    if [ ! -h "${repository}/hooks/post-update.d/${remote}_${type}-post-update.hook" ]; then
        /bin/ln -s "${tails_common_hookdir}/${remote}_${type}-post-update.hook" \
		   "${repository}/hooks/post-update.d/${remote}_${type}-post-update.hook"
    fi
}

deinstall_post_update_hook () {
    repository="$1"
    type="$2"
    remote="$3"
    link="${repository}/hooks/post-update.d/${remote}_${type}-post-update.hook"
    if [ -h "$link" ]; then
        rm "$link"
    fi
}

install_mirror_post_update_hook () {
    repository="$1"
    remote="$2"
    install_post_update_hook "$repository" 'mirror' "$remote"
}

install_website_ping_post_update_hook () {
    repository="$1"
    remote="$2"
    install_post_update_hook "$repository" 'website_ping' "$remote"
}

install_website_underlays_post_update_hook () {
    repository="$1"
    remote="$2"
    install_post_update_hook "$repository" 'website_underlays' "$remote"
}

/usr/bin/find "$repository_root" \
   -mindepth 1 -maxdepth 1 \
   -type d -print0 | while IFS= read -r -d '' repository ; do
    case $(basename "$repository") in
        etcher-binary.git|mirror-pool-dispatcher.git|mirror-pool.git|promotion-material.git)
            install_post_update_dot_d_hook "$repository"
            install_website_underlays_post_update_hook "$repository" www
	    ;;
        jenkins-jobs.git)
            install_post_update_dot_d_hook "$repository"
            install_mirror_post_update_hook "$repository" gitlab
            ;;
        puppet-*)
            if ! grep -qs -x "$(basename "$repository")" \
                 /var/lib/gitolite3/projects.list ; then
                continue
            fi
            install_post_update_dot_d_hook "$repository"
            install_mirror_post_update_hook "$repository" gitlab
            ;;
        tails.git)
            install_post_update_dot_d_hook "$repository"
            install_website_ping_post_update_hook "$repository" www
            install_mirror_post_update_hook "$repository" salsa
            ;;
        *)
            ;;
    esac
done
