error() {
   echo "$*" >&2
   exit 2
}

ref_name_to_suite() {
   local ref="$1"

   echo "$ref" | sed -e 's,[^a-z0-9.-],-,ig' | tr '[A-Z]' '[a-z]'
}

assert_is_branch_name() {
   local branch="$1"

   if ! echo "$branch" | grep -q -E "$BRANCH_RE" ; then
       error "Invalid branch name: '$branch'"
   fi
}

assert_is_apt_suite() {
   local suite="$1"

   if ! echo "$suite" | grep -q -E "$APT_SUITE_RE" ; then
       error "Invalid APT suite name: '$suite'"
   fi
}

assert_is_list_of_apt_suites() {
   for suite in "$@" ; do
      assert_is_apt_suite "$suite"
   done
}

is_tag() {
   local rev="$1"

   echo "$rev" | grep -q -E "$TAG_RE"
}

apt_overlays_in_branch() {
   local branch
   branch="$1"

   git ls-tree --full-tree --name-only -r "$branch" config/APT_overlays.d \
       | xargs --max-args=1 basename  \
       | grep --line-regexp -v '.placeholder' \
       || :
}
